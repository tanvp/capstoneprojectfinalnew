﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class EmployeeWorkingShiftRegitserApi
    {
        public IEnumerable<EmployeeWorkingShiftRegitser> GetAll()
        {
            return this.BaseService.Get().Where(e=>e.IsActive == true);
        }
    }
}
