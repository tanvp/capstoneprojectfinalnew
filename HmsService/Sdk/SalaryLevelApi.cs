﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class SalaryLevelApi
    {
        public List<SalaryLevel> GetSalaryLevelById(int id)
        {
            return this.BaseService.Get(s => s.Id == id).Where(s => s.IsActive).ToList();
        }
    }
}
