﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class EmployeeWorkingShiftAutoMappingApi
    {
        public IEnumerable<EmployeeWorkingShiftAutoMapping> GetAll()
        {
            return this.BaseService.Get();
        }
    }
}
