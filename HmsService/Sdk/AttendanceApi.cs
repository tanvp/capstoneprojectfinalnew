﻿using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class AttendanceApi
    {
        public IEnumerable<Attendance> GetAll()
        {
            return this.BaseService.Get();
        }
        public IEnumerable<Attendance> GetAllAttendanceActive() {
            return this.BaseService.Get().Where(a=>a.IsActive ==true);
        }
        public IEnumerable<Attendance> GetAttendanceByStoreByTimeRange(int storeId, DateTime startTime, DateTime endTime)
        {
            return this.BaseService.Get(q => q.IsActive == true && q.Date >= startTime && q.Date <= endTime && q.Employee.StoreId == storeId);
        }
        public IEnumerable<Attendance> GetAttendanceByTimeRangeAndBrand(List<int> listStore, DateTime startTime, DateTime endTime)
        {
            return this.BaseService.Get(q => q.IsActive == true &&   q.Date >= startTime && q.Date <= endTime && listStore.Any(d => d == q.Employee.StoreId));
        }
        public IQueryable<Attendance> GetAttendanceByTimeRangeAndStore(int storeId, DateTime startDate, DateTime endDate)
        {
            return this.BaseService.GetActive(q => q.IsActive ==true && q.Employee.StoreId == storeId && q.Date >= startDate && q.Date <= endDate);
        }
        public IQueryable<Attendance> GetAttendanceByTimeRangeAndStore(int storeId, TimeSpan startTime, TimeSpan endTime, DateTime date)
        {
            return this.BaseService.GetActive(q => q.IsActive == true && q.Employee.StoreId == storeId && q.ShiftMin >= startTime && q.ShiftMin <= endTime && q.Date == date);
        }
        public IEnumerable<Attendance> GetAttendanceByTimeRangeAndStore2(int storeId, DateTime startDate, DateTime endDate)
        {
            IEnumerable<Attendance> result = this.BaseService.Get(q => q.IsActive == true && q.Employee.StoreId == storeId && ((q.ShiftMin >= startDate.TimeOfDay && q.ShiftMin <= endDate.TimeOfDay) || (q.ShiftMax >= startDate.TimeOfDay && q.ShiftMax <= endDate.TimeOfDay)) && q.Date == startDate.Date);
            return result;
        }
    }
}
