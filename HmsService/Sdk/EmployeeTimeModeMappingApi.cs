﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class EmployeeTimeModeMappingApi
    {
        public IEnumerable<EmployeeTimeModeMapping> GetAll()
        {
            return this.BaseService.Get();
        }
        public IEnumerable<EmployeeTimeModeMapping> GetAllEmployeeTimeModeMappingActive()
        {
            return this.BaseService.Get().Where(a => a.IsActive == true);
        }
        public IEnumerable<EmployeeTimeModeMapping> GetByStoreByTimeRange(int storeId, DateTime startTime, DateTime endTime)
        {
            return this.BaseService.Get(q => q.IsActive == true && q.Date >= startTime && q.Date <= endTime && q.Employee.StoreId == storeId);
        }
        public IEnumerable<EmployeeTimeModeMapping> GetByTimeRangeAndBrand(List<int> listStore, DateTime startTime, DateTime endTime)
        {
            return this.BaseService.Get(q => q.IsActive == true && q.Date >= startTime && q.Date <= endTime && listStore.Any(d => d == q.Employee.StoreId));
        }

    }
}
