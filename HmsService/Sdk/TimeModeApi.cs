﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class TimeModeApi
    {
        public IEnumerable<TimeMode> GetAll()
        {
            return this.BaseService.GetAllTimeMode().ToList();
            //return this.BaseService.Get(t => t.IsActive);
        }

        //public TimeModeEditViewModel GetTimeModeById(int id)
        public IEnumerable<TimeMode> GetTimeModeById(int id)
        {
            //var timeMode = this.BaseService.Get()?.ToViewModel<TimeMode, TimeModeViewModel>();
            //var timeModeEditViewModel = new TimeModeEditViewModel()
            //{
            //    TimeModeViewModel = timeMode,
            //};
            //return timeModeEditViewModel;
            return this.BaseService.Get().Where(t => t.Id == id && t.IsActive == true);
        }
        
    }
}
