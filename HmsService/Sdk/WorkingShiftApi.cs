﻿using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class WorkingShiftApi
    {
        public IEnumerable<WorkingShift> GetAll()
        {
            return this.BaseService.GetAllWorkingShift();
        }
        
        public IEnumerable<WorkingShift> GetAllWorkingShiftByid(int id)
        {
            // return this.BaseService.GetAllWorkingShiftByID(id);
            return BaseService.Get(q => q.Id == id && q.IsActive == true);
        }
        //public async Task EditWorkingShiftAsync(WorkingShiftViewModel model)
        //{
        //    var wksApi = new WorkTypeApi();
        //    var wks = await this.BaseService.GetAsync(model.Id);
        //    wks.WorkingShiftName = model.WorkingShiftName;
        //    wks.ShiftMin = model.ShiftMin;
        //    wks.ShiftMax = model.ShiftMax;
        //    wks.ExpectedHoursWork = model.ExpectedHoursWork;
        //    await this.BaseService.UpdateAsync(wks);
        //}
        //public WorkingShiftViewModel GetWorkingShiftById(int id)
        //{
        //    var wks = this.BaseService.Get(id);
        //    if (wks == null)
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        return new WorkingShiftViewModel(wks);
        //    }
        //}

    }
}
