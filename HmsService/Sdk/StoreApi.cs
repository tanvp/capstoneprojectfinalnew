﻿using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class StoreApi
    {
        public IEnumerable<Store> GetAll()
        {
            return this.BaseService.Get();
        }
        public IQueryable<StoreViewModel> GetActiveStoreByBrandId(int brandId)
        {
            return this.BaseService.Get().Where(s=>s.isActive == true && s.BrandId == brandId).Select(s=>new StoreViewModel {
                Id = s.Id,
                BrandId = s.BrandId,
                Name = s.Name,
                Address = s.Address,
                isActive = s.isActive
            });
        }
        public async Task<StoreViewModel> GetStoreByID(int storeid)
        {
            var store = await this.BaseService.GetStoreByID(storeid);
            if (store == null)
            {
                return null;
            }
            else
            {
                return new StoreViewModel(store);
            }
        }
    }
}
