﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class DayTypeApi
    {
        public IEnumerable<HmsService.Models.Entities.DayType> GetAll()
        {
            return this.BaseService.GetAllDayType();
        }
    }
}
