﻿using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class EmployeeApi
    {
        public IEnumerable<Employee> GetAll()
        {
            return this.BaseService.GetAllEmployees();
        }
        public IEnumerable<Employee> GetAllEmpoyeeByStoreId(int storeId)
        {
            return this.BaseService.GetAllEmployees().Where(e=>e.StoreId==storeId);
        }
    }
}
