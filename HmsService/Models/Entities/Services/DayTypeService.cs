﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IDayTypeService
    {

        IEnumerable<DayType> GetAllDayType();

    }

    public partial class DayTypeService
    {
        public IEnumerable<DayType> GetAllDayType()
        {
            IEnumerable<DayType> dayTypeList = this.Get().Where(d => d.IsActive);
            return dayTypeList;
        }
    }
}
