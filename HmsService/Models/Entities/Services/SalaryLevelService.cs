﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface ISalaryLevelService
    {
        IEnumerable<SalaryLevel> GetAllSalaryLevels();
    }
    public partial class SalaryLevelService
    {
        public IEnumerable<SalaryLevel> GetAllSalaryLevels()
        {
            return this.Get(q => q.IsActive == true);
        }
    }
}
