﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IWorkingShiftService
    {

        IEnumerable<WorkingShift> GetAllWorkingShift();
        IEnumerable<WorkingShift> GetAllWorkingShiftByID(int Id);

    }
    public partial class WorkingShiftService
    {
        public IEnumerable<WorkingShift> GetAllWorkingShift()
        {
            return this.Get(q => q.IsActive == true);
        }
        public IEnumerable<WorkingShift> GetAllWorkingShiftByID(int Id)
        {
            return this.Get(q => q.Id==Id && q.IsActive);
        }
    }

}
