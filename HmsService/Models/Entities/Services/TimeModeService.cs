﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface ITimeModeService
    {

        IEnumerable<TimeMode> GetAllTimeMode();

    }

    public partial class TimeModeService
    {
        public IEnumerable<TimeMode> GetAllTimeMode()
        {
            IEnumerable<TimeMode> timeModeList = this.Get().Where(t => t.IsActive);
            return timeModeList;
        }

    }
}
