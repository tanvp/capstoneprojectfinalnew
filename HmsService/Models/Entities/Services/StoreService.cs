﻿using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{

    public partial interface IStoreService
    {
        Task<Store> GetStoreByID(int storeid);

    }

    public partial class StoreService
    {
        public async Task<Store> GetStoreByID(int storeid)
        {
            return await this.GetAsync(storeid);
        }
    }

    public enum StoreSortableProperty
    {
        Id,
        Name,
        ShortName,
        HasNews,
        HasProducts,
        HasImageCollections,
    }

}
