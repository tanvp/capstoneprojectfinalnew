﻿using HmsService.Models.Entities.Repositories;
using SkyWeb.DatVM.Data;
using SkyWeb.DatVM.Mvc.Autofac;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{

    public partial interface IAspNetUserService
    {
        AspNetUser CheckLogin(string username, string password);
    }

    public partial class AspNetUserService
    {
        public AspNetUser CheckLogin(string username, string password)
        {
            return this.Get(a => a.UserName.Equals(username) && a.PasswordHash.Equals(password)).FirstOrDefault();
        }
        //public AspNetUser GetUserByUsername(string username)
        //{

        //    return this.Get(a => a.UserName.Equals(username)).FirstOrDefault();
        //}
    }

    public class AspNetUserDetails : IEntity
    {

        public AspNetUser AspNetUser { get; set; }
        public ICollection<AspNetRole> Roles { get; set; }
        public string AdminStoreName { get; set; }
        public int? AdminStoreId { get; set; }

    }

}
