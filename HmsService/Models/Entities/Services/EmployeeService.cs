﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IEmployeeService
    {

        IEnumerable<Employee> GetAllEmployees();
    }

    public partial class EmployeeService
    {
        public IEnumerable<Employee> GetAllEmployees()
        {
            return this.Get(q => q.IsActive == true);
        }
    }
}
