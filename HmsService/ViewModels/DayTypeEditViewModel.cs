﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HmsService.Models;

namespace HmsService.ViewModels
{
    public partial class DayTypeEditViewModel
    {
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableDayTypeList { get; set; }
        public DayTypeViewModel DayTypeViewModel { get; set; }
        public string[] WeekDayNames { get; set; }

    }

}
