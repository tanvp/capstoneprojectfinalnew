//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HmsService.ViewModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalaryLevelViewModel : SkyWeb.DatVM.Mvc.BaseEntityViewModel<HmsService.Models.Entities.SalaryLevel>
    {
    	
    			public virtual int Id { get; set; }
    			public virtual string SalaryLevelName { get; set; }
    			public virtual Nullable<double> SalaryValue { get; set; }
    			public virtual int BrandId { get; set; }
    			public virtual bool IsActive { get; set; }
    	
    	public SalaryLevelViewModel() : base() { }
    	public SalaryLevelViewModel(HmsService.Models.Entities.SalaryLevel entity) : base(entity) { }
    
    }
}
