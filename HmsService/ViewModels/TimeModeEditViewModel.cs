﻿using HmsService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HmsService.ViewModels
{
    public partial class TimeModeEditViewModel
    {
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableTimeModeList { get; set; }
        public TimeModeViewModel TimeModeViewModel { get; set; }
        public PriorityEnum TimeModePriorityEnum { get; set; }
    }
}
