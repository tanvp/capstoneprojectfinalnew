﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class SalaryLevelEditViewModel
    {
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableBrand { get; set; }
        public SalaryLevelViewModel SalaryLevelViewModel { get; set; }
    }
}
