﻿using HmsService.Models;
using HmsService.Models.Entities;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class WorkingShiftEditViewModel
    {
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableBrand { get; set; }
        public WorkingShiftViewModel WorkingShiftViewModel { get; set; }
        public WorkingShiftTypeEnum WorkingShiftTypeSelected { get; set; }
    }
   
}
