//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HmsService.ViewModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttendanceViewModel : SkyWeb.DatVM.Mvc.BaseEntityViewModel<HmsService.Models.Entities.Attendance>
    {
    	
    			public virtual int Id { get; set; }
    			public virtual Nullable<int> EmployeeId { get; set; }
    			public virtual Nullable<int> WorkingShiftType { get; set; }
    			public virtual Nullable<System.DateTime> Date { get; set; }
    			public virtual Nullable<System.TimeSpan> CheckMin { get; set; }
    			public virtual Nullable<System.TimeSpan> CheckMax { get; set; }
    			public virtual Nullable<System.TimeSpan> ShiftMin { get; set; }
    			public virtual Nullable<System.TimeSpan> ShiftMax { get; set; }
    			public virtual Nullable<System.TimeSpan> ExpandTime { get; set; }
    			public virtual Nullable<int> Status { get; set; }
    			public virtual string UpdatePerson { get; set; }
    			public virtual string ImageUrl { get; set; }
    			public virtual Nullable<double> Confidence { get; set; }
    			public virtual Nullable<bool> IsActive { get; set; }
    	
    	public AttendanceViewModel() : base() { }
    	public AttendanceViewModel(HmsService.Models.Entities.Attendance entity) : base(entity) { }
    
    }
}
