﻿using HmsService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class EmployeeEditViewModel
    {
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableSalaryLevels { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> AvailableStores { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Select a correct role")]
        public RoleEnum RoleSelected { get; set; }
        public EmployeeViewModel EmployeeViewModel { get; set; }
        
    }
}
