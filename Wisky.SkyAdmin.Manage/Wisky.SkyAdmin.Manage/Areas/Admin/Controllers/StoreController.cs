﻿using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class StoreController : Controller
    {
        // GET: Store
        public ActionResult GetListStore()
        {
            var brandId = 1;
            var storeApi = new StoreApi();
            var listStore = storeApi.GetActiveStoreByBrandId(brandId).ToList();
            var result = listStore.Select(q => new
            {
                storeId = q.Id,
                storeName = q.Name
            });
            return Json(new { listresult = result }, JsonRequestBehavior.AllowGet);
        }

    }
}