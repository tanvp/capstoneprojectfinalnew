﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class ImageController : DomainBasedController
    {
        public async Task<ActionResult> Edit(int id)
        {
            var info = new BlogPostApi()
                .GetDetailsByStoreId(id, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new BlogPostEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        public async Task<ActionResult> Create(int brandId)
        {
            var storeId = 0;
            if (RouteData.Values["storeId"] != null)
            {
                storeId = int.Parse(RouteData.Values["storeId"]?.ToString());
            }

            if (storeId == 0 && brandId == 0)
            {
                return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            }

            var model = new BlogPostEditViewModel()
            {
                BlogPost = new BlogPostViewModel(),
            };

            model.BlogPost.BrandId = brandId;
            model.BlogPost.Author = System.Web.HttpContext.Current.User.Identity.Name;
            model.BlogPost.CreatedTime = DateTime.Today;
            model.BlogPost.StartDate = DateTime.Today;
            model.BlogPost.EndDate = DateTime.Today.AddDays(1);
            model.BlogPost.LastUpdatePerson = System.Web.HttpContext.Current.User.Identity.Name;
            model.BlogPost.Position = 0;
            model.BlogPost.TotalVisit = 0;
            model.BlogPost.BlogType = (int)BlogTypeEnum.Adsvertisement;

            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> Create(BlogPostEditViewModel model, int storeId, int brandId)
        {
            if (storeId == 0 && brandId == 0)
            {
                return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }
            BlogCategoryApi blogCategoryApi = new BlogCategoryApi();

            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.BrandId = this.CurrentStore.BrandId.Value;
            model.BlogPost.Active = true;
            model.BlogPost.Author = this.User.Identity.Name;
            model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
            model.BlogPost.CreatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.BlogType = (int)BlogTypeEnum.Logo;
            model.BlogPost.Active = false;
            model.BlogPost.SeoName = "img";
            model.BlogPost.Status = (int)BlogStatusEnum.Approve;

            var blogCategory = blogCategoryApi.GetIdCateByType((int)BlogCateTypeEnum.Resource, brandId).FirstOrDefault();// for save(entity)
            model.BlogPost.BlogCategoryId = blogCategory;

            if (model.SelectedImages == null)
            {
                string[] arr = { model.BlogPost.Image };
                model.SelectedImages = arr;
            }
            var blogPostApi = new BlogPostApi();
            await blogPostApi.CreateAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0],
                model.SelectedTags ?? new int[0]);

            return this.RedirectToAction("Index", "ImageCollection", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));

            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);

        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogPostEditViewModel model)
        {
            var api = new BlogPostApi();
            // Validate
            var info = api
                .GetByStoreAsyn(model.BlogPost.Id, this.CurrentStore.ID);


            BlogPostCollectionApi blogPostCollectionApi = new BlogPostCollectionApi();
            //var selectedCollections = blogPostCollectionApi.GetSelectedCollection(model.SelectedBlogPostCollections);
            List<BlogPostCollection> selectedCollections = null;
            int collectionNumber = 0;
            if (model.SelectedBlogPostCollections != null)
            {
                selectedCollections = blogPostCollectionApi.GetSelectedCollection(model.SelectedBlogPostCollections);
                collectionNumber = CalculateCollectionNumber(selectedCollections);
            }


            if (info == null)
            {
                return this.IdNotFound();
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }
            model.BlogPost.Status = info.Status;
            if (info.Status == (int)BlogStatusEnum.Processing)
            {
                model.BlogPost.UserApprove = null;
            }
            model.BlogPost.StoreId = info.StoreId;
            model.BlogPost.BrandId = info.BrandId;
            model.BlogPost.CreatedTime = info.CreatedTime;
            model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.Active = info.Active;
            model.BlogPost.Author = info.Author;
            model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
            model.BlogPost.AuthorRefer = info.AuthorRefer;
            model.BlogPost.BlogType = (int)BlogTypeEnum.Logo;

            if (model.SelectedImages == null)
            {
                string[] arr = { model.BlogPost.Image };
                model.SelectedImages = arr;
            }
            await api.EditAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0],
                model.SelectedTags ?? new int[0]);

            return this.RedirectToAction("Index", "ImageCollection", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));

            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedBlogPostCollections.Contains(q.Id));

        }

        public int CalculateCollectionNumber(List<BlogPostCollection> collections)
        {
            int total = 0;
            foreach (var item in collections)
            {
                total += (int)Math.Pow(2, (int)item.Position);
            }
            return total;
        }
    }
}