﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static HmsService.ViewModels.EmployeeEditViewModel;

namespace Admin_Capstone_KPS.Areas.Admin.Controllers
{
    public class EmployeeController : Controller
    {

        HmsEntities entities = new HmsEntities();
        // GET: Employee

        public ActionResult Index(int storeId, int brandId)
        {
            StoreApi storeApi = new StoreApi();
            var storeList = storeApi.Get().Select(d => new SelectListItem
            {
                Text = d.Name,
                Value = d.Id.ToString(),
            }).ToList();
            SalaryLevelApi salaryLevelApi = new SalaryLevelApi();
            var salaryLevelList = salaryLevelApi.Get().Select(c => new SelectListItem
            {
                Text = c.SalaryLevelName,
                Value = c.Id.ToString(),
            });

            EmployeeEditViewModel employeeEditViewModel = new EmployeeEditViewModel()
            {
                AvailableSalaryLevels = salaryLevelList,
                AvailableStores = storeList,
            };
            ViewBag.AvailableStores = storeList;
            return View("Index", employeeEditViewModel);

        }
        public ActionResult Create()
        {
            StoreApi storeApi = new StoreApi();
            var storeList = storeApi.Get().Select(d => new SelectListItem
            {
                Text = d.Name,
                Value = d.Id.ToString(),
            }).ToList();
            SalaryLevelApi salaryLevelApi = new SalaryLevelApi();
            var salaryLevelList = salaryLevelApi.Get().Select(c => new SelectListItem
            {
                Text = c.SalaryLevelName,
                Value = c.Id.ToString(),
            });

            EmployeeEditViewModel employeeEditViewModel = new EmployeeEditViewModel()
            {
                AvailableSalaryLevels = salaryLevelList,
                AvailableStores = storeList,
            };

            return View("Create", employeeEditViewModel);
        }
        public ActionResult CreateEmployee(EmployeeEditViewModel emp)
        {
            try
            {
                EmployeeApi employeeApi = new EmployeeApi();
                emp.EmployeeViewModel.Role = (int)emp.RoleSelected;
                emp.EmployeeViewModel.IsActive = true;
                if (CheckPin(emp.EmployeeViewModel.PIN) == true)
                {
                    employeeApi.Create(emp.EmployeeViewModel);
                }
                else
                {
                    return RedirectToAction("Create");
                }
            }
            catch (Exception e)
            {
            }
            return RedirectToAction("Index");
        }
        public JsonResult GetEmployees(JQueryDataTableParamModel param)
        {
            try
            {
                var employeeApi = new EmployeeApi();
                var listEmp = employeeApi.GetAll().ToList();
                var count = 0;
                var em2 = listEmp.Select(e => new object[]
                {
                   ++count,
                e.Id,
                e.Store !=null ? e.Store.Name : "",
                e.Username!= null ? e.Username : "",
                e.Password!= null ? e.Password : "",
                e.EmployeeName!= null ? e.EmployeeName : "",
                e.DateOfBirth!= null ? e.DateOfBirth.GetValueOrDefault().ToString("dd/MM/yyyy") : "",
                e.Address != null ? e.Address : "",
                e.Phone != null ? e.Phone : "",
                ((RoleEnum)e.Role).DisplayName(),
                e.SalaryLevel.SalaryLevelName,
                e.HoursWorked !=null ? e.HoursWorked :0,
                e.FaceId != null ?e.FaceId:"",
                e.PIN != null ?e.PIN:"",
                e.StoreId,
                e.Role,
                e.SalaryLevelId,
                }).ToList();

                var totalRecords = em2.Count();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = em2
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public JsonResult GetEmployeesForTimeRules(JQueryDataTableParamModel param)
        {
            try
            {
                var employeeApi = new EmployeeApi();
                var listEmp = employeeApi.GetAll().ToList();
                var count = 0;
                var em2 = listEmp.Select(e => new object[]
                {
                ++count,
                e.Id,
                e.EmployeeName!= null ? e.EmployeeName : "",
                e.Phone != null ? e.Phone : "",
                }).ToList();
                var totalRecords = em2.Count();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = em2
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        [HttpPost]
        public async Task<JsonResult> EditEmployees(int Id, int storeId, string userName, string passWord, string name, string dateofbirthe, string addresse, string phonee, int rolee, int salarylevele, double houre, string faceIde, string pin)
        {
            bool result = false;
            try
            {
                var empApi = new EmployeeApi();
                var emp = await empApi.GetAsync(Id);
                emp.StoreId = storeId;
                emp.Username = userName;
                emp.Password = passWord;
                emp.EmployeeName = name;
                emp.DateOfBirth = DateTime.ParseExact(dateofbirthe, "dd/MM/yyyy", null);
                emp.Address = addresse;
                emp.Phone = phonee;
                emp.Role = rolee;
                emp.SalaryLevelId = salarylevele;
                emp.HoursWorked = houre;
                emp.FaceId = faceIde;
                emp.PIN = pin;
                await empApi.EditAsync(Id, emp);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return Json(result);
        }


        [HttpPost]
        public async Task<JsonResult> DeleteEmployee(int? Id)
        {
            try
            {
                var empApi = new EmployeeApi();
                var model = await empApi.GetAsync(Id);
                if (model == null || !model.IsActive)
                {
                    return Json(new { success = false });
                }
                model.IsActive = false;
                await empApi.EditAsync(model.Id, model);
                return Json(new { success = true });
            }
            catch (System.Exception e)
            {
                return Json(new { success = false });
            }
        }

        public Boolean CheckPin(string pin)
        {
            //HmsEntities entities = new HmsEntities();
            EmployeeApi employeeApi = new EmployeeApi();
            bool isValid = !employeeApi.GetAll().ToList().Exists(emp => emp.PIN.Equals(pin, StringComparison.CurrentCultureIgnoreCase));
            return isValid;
        }
        [HttpPost]
        public JsonResult CheckPin1(string pin)
        {
            HmsEntities entities = new HmsEntities();

            bool isValid = !entities.Employees.ToList().Exists(emp => emp.PIN.Equals(pin, StringComparison.CurrentCultureIgnoreCase));
            return Json(isValid);
        }
    }
}