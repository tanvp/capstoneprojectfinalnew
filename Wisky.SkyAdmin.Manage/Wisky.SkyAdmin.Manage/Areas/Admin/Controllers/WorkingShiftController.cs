﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
  // GET: Admin/WorkingShift
        public class WorkingShiftController : Controller
        {
            HmsEntities entities = new HmsEntities();
            // GET: WorkingShift
            public ActionResult Index()
            {
                BrandApi brandApi = new BrandApi();
                var wksList = brandApi.Get().Select(c => new SelectListItem
                {
                    Text = c.BrandName,
                    Value = c.Id.ToString(),
                });

                WorkingShiftEditViewModel wksEditViewModel = new WorkingShiftEditViewModel()
                {
                    AvailableBrand = wksList,

                };
                return View("Index", wksEditViewModel);

            }
            public ActionResult Create()
            {
                BrandApi brandApi = new BrandApi();
                var wksList = brandApi.Get().Select(c => new SelectListItem
                {
                    Text = c.BrandName,
                    Value = c.Id.ToString(),
                });

                WorkingShiftEditViewModel wksEditViewModel = new WorkingShiftEditViewModel()
                {
                    AvailableBrand = wksList,

                };
                return View("Create", wksEditViewModel);

            }
            public ActionResult CreateWorkingShift(WorkingShiftEditViewModel wks)
            {
                try
                {
                    WorkingShiftApi wksApi = new WorkingShiftApi();
                    wks.WorkingShiftViewModel.IsActive = true;
                    wks.WorkingShiftViewModel.WorkingShiftType = (int)wks.WorkingShiftTypeSelected;
                    wksApi.Create(wks.WorkingShiftViewModel);
                }
                catch (Exception e)
                {
                }
                return RedirectToAction("Index");
            }

            public JsonResult GetWorkingShifts()
            {
                try
                {
                    var workingShiftApi = new WorkingShiftApi();
                    var listWorkingShift = workingShiftApi.GetAll().ToList();

                    var count = 0;
                    //var wks = listWorkingShift.Select(e => new object[]
                    //{
                    //e.Id,
                    //e.Brand.BrandName,
                    //e.WorkingShiftName!= null ? e.WorkingShiftName : "",
                    //e.ShiftMin!= null ? e.ShiftMin.GetValueOrDefault().ToString(@"hh\:mm") : "",
                    //e.ShiftMax!= null ? e.ShiftMax.GetValueOrDefault().ToString(@"hh\:mm") : "",
                    //e.FromDate!= null ? e.FromDate.GetValueOrDefault().ToShortDateString() : "",
                    //e.ToDate!= null ? e.ToDate.GetValueOrDefault().ToShortDateString() : "",
                    //e.ExpandTime !=null ? e.ExpandTime.GetValueOrDefault().ToString(@"hh\:mm"):"",
                    //e.BrandId,
                    //}).ToList();

                    var wks = listWorkingShift.Select(e => new WorkingShiftEditViewModel2
                    {
                        Id = e.Id,
                        BrandId = e.Brand.Id,
                        WorkingShiftName = e.WorkingShiftName != null ? e.WorkingShiftName : "",
                        ShiftMin = e.ShiftMin.GetValueOrDefault(),
                        ShiftMax = e.ShiftMax.GetValueOrDefault(),
                        ExpandTime = e.ExpandTime.GetValueOrDefault(),
                    }).ToList();


                    var totalRecords = wks.Count();
                    return Json(new
                    {
                        aaData = wks
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            public JsonResult GetWorkingShift(HmsService.Models.JQueryDataTableParamModel param)
            {
                try
                {
                    var workingShiftApi = new WorkingShiftApi();
                    var listWorkingShift = workingShiftApi.GetAll().ToList();

                    var count = 0;
                    var wks = listWorkingShift.Select(e => new object[]
                    {
                ++count, //0
                e.Id, //1
                e.Brand.BrandName, //2
                e.WorkingShiftName!= null ? e.WorkingShiftName : "", //3
                e.ShiftMin!= null ? e.ShiftMin.GetValueOrDefault().ToString(@"hh\:mm") : "", //4
                e.ShiftMax!= null ? e.ShiftMax.GetValueOrDefault().ToString(@"hh\:mm") : "", //5
                ((WorkingShiftTypeEnum)e.WorkingShiftType).DisplayName(), //6
                e.ExpandTime !=null ? e.ExpandTime.GetValueOrDefault().ToString(@"hh\:mm"):"", //7
                e.BrandId,  //8
                e.WorkingShiftType,//9
                    }).ToList();

                    var totalRecords = wks.Count();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalRecords = totalRecords,
                        iTotalDisplayRecords = totalRecords,
                        aaData = wks
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            [HttpPost]
            public async Task<JsonResult> EditWorkingShift(int Id, int brandId, string name, string shiftMin, string shiftMax, int wksType, string hour)
            {
                bool result = false;
                try
                {
                    var wksApi = new WorkingShiftApi();
                    var wks = await wksApi.GetAsync(Id);
                    wks.BrandId = brandId;
                    wks.WorkingShiftName = name;
                    wks.ShiftMin = TimeSpan.Parse(shiftMin);
                    wks.ShiftMax = TimeSpan.Parse(shiftMax);
                    wks.WorkingShiftType = wksType;
                    wks.ExpandTime = TimeSpan.Parse(hour);
                    await wksApi.EditAsync(Id, wks);
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
                return Json(result);
            }
            [HttpPost]
            public async Task<JsonResult> DeleteWorkingShift(int? Id)
            {
                try
                {
                    var wksApi = new WorkingShiftApi();
                    var model = await wksApi.GetAsync(Id);
                    if (model == null || !model.IsActive)
                    {
                        return Json(new { success = false });
                    }
                    model.IsActive = false;
                    await wksApi.EditAsync(model.Id, model);
                    return Json(new { success = true });
                }
                catch (System.Exception e)
                {
                    return Json(new { success = false });
                }
            }
            [ValidateAntiForgeryToken]
            public ActionResult Edit([Bind(Include = "Id,WorkingShiftName,ShiftMin,ShiftMax,ExpectedHoursWork")] WorkTypeViewModel wks)
            {
                if (ModelState.IsValid)
                {
                    entities.Entry(wks).State = EntityState.Modified;
                    entities.SaveChanges();

                    string url = Url.Action("Index", "Addresses", new { id = wks.Id });
                    return Json(new { success = true, url = url });
                }


                return PartialView("_Edit", wks);
            }
        }
    public class WorkingShiftEditViewModel2
    {
        public virtual int Id { get; set; }
        public virtual Nullable<int> BrandId { get; set; }
        public virtual string WorkingShiftName { get; set; }
        public virtual Nullable<int> WorkingShiftType { get; set; }
        public virtual Nullable<System.TimeSpan> ShiftMin { get; set; }
        public virtual Nullable<System.TimeSpan> ShiftMax { get; set; }
        public virtual string FromDate { get; set; }
        public virtual string ToDate { get; set; }
        public virtual Nullable<System.TimeSpan> ExpandTime { get; set; }
    }
}
   
