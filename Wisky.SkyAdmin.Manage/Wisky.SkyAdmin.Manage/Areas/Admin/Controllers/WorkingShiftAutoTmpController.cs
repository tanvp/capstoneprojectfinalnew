﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class WorkingShiftAutoTmpController : Controller
    {
        // GET: WorkingShiftAutoTmp
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetWorkingShifts()
        {
            try
            {
                var workingShiftAutoTmpApi = new WorkingShiftAutoTmpApi();
                var listWorkingShift = workingShiftAutoTmpApi.GetAll().ToList();

                var count = 0;
                //var wks = listWorkingShift.Select(e => new object[]
                //{
                //e.Id,
                //e.Brand.BrandName,
                //e.WorkingShiftName!= null ? e.WorkingShiftName : "",
                //e.ShiftMin!= null ? e.ShiftMin.GetValueOrDefault().ToString(@"hh\:mm") : "",
                //e.ShiftMax!= null ? e.ShiftMax.GetValueOrDefault().ToString(@"hh\:mm") : "",
                //e.FromDate!= null ? e.FromDate.GetValueOrDefault().ToShortDateString() : "",
                //e.ToDate!= null ? e.ToDate.GetValueOrDefault().ToShortDateString() : "",
                //e.ExpandTime !=null ? e.ExpandTime.GetValueOrDefault().ToString(@"hh\:mm"):"",
                //e.BrandId,
                //}).ToList();

                var wks = listWorkingShift.Select(e => new WorkingShiftAutoTmpEditViewModel2
                {
                    Id = e.Id,
                    Name = e.Name,
                    TimeStart = e.TimeStart.GetValueOrDefault(),
                    TimeEnd = e.TimeEnd.GetValueOrDefault(),
                    Date = e.Date != null ? e.Date.GetValueOrDefault().ToShortDateString() : "",
                    EmptySlot = e.EmptySlot,
                    TimeExpand = e.TimeExpand,
                    IsActive = e.IsActive
                }).ToList();


                var totalRecords = wks.Count();
                return Json(new
                {
                    aaData = wks
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult AutoAssign()
        {
            DateTime fromDate = new DateTime(2018, 07, 09);
            DateTime toDate = new DateTime(2018, 07, 16);
            //get wishlist from db 
            WorkingShiftAutoTmpApi workingShiftAutoTmpApi = new WorkingShiftAutoTmpApi();
            EmployeeWorkingShiftRegitserApi employeeWorkingShiftRegitserApi = new EmployeeWorkingShiftRegitserApi();
            EmployeeWorkingShiftAutoMappingApi employeeWorkingShiftAutoMappingApi = new EmployeeWorkingShiftAutoMappingApi();
            EmployeeApi employeeApi = new EmployeeApi();
            var listEmployee = employeeApi.GetAll().ToList();
            var listWorkShiftAuto = workingShiftAutoTmpApi.GetAll().Where(w => BetweenDate(w.Date.GetValueOrDefault(), fromDate, toDate)
                                                                            && w.IsActive == true);
            List<EmployeeWorkingShiftAutoMappingViewModel> listResult = new List<EmployeeWorkingShiftAutoMappingViewModel>();

            foreach (var workingShiftAuto in listWorkShiftAuto)
            {
                //get wishlist from db 
                listEmployee = employeeApi.GetAll().ToList();
                var wishList = employeeWorkingShiftRegitserApi.GetAll().Where(e => e.IsActive == true
                                                                                && e.Date == workingShiftAuto.Date
                                                                                && e.WorkingShift.ShiftMin == workingShiftAuto.TimeStart
                                                                                && e.WorkingShift.ShiftMax == workingShiftAuto.TimeEnd).OrderByDescending(e => e.TimeCreated).ToList();

                if (wishList != null)
                {
                    var employeeInWishList = listEmployee.Where(e => wishList.Any(x => x.EmployeeId == e.Id));

                    foreach (var item in PutEmloyeeIntoShiftYouWish(workingShiftAuto, employeeInWishList.ToList(), listEmployee))
                    {
                        listResult.Add(item);
                    };
                }
                //Trường hợp ko có wishList cho ca đó
                else
                {
                    //Lấy các record cũ bỏ vào
                    var historyShiftAuto = workingShiftAutoTmpApi.GetAll().Where(w => w.Date == workingShiftAuto.Date.GetValueOrDefault().AddDays(-7)
                                                                                    && w.TimeStart == workingShiftAuto.TimeStart
                                                                                    && w.TimeEnd == workingShiftAuto.TimeEnd).FirstOrDefault();
                    if (historyShiftAuto != null)
                    {
                        var employeeInWishList = listEmployee.Where(e => historyShiftAuto.EmployeeWorkingShiftAutoMappings.Any(x => x.EmployeeId == e.Id));
                        foreach (var item in PutEmloyeeIntoShiftYouWish(workingShiftAuto, employeeInWishList.ToList(), listEmployee))
                        {
                            listResult.Add(item);
                        };
                    }
                    else
                    {
                        foreach (var item in PutEmloyeeIntoShiftYouWish(workingShiftAuto, new List<Employee>(), listEmployee))
                        {
                            listResult.Add(item);
                        };
                    }

                }
            }
            if (listResult.Count > 0)
            {
                foreach (var item in listResult)
                {
                    employeeWorkingShiftAutoMappingApi.Create(item);
                }
            }
            return View("Index");
        }

        public void AcceptAutoShift()
        {
            DateTime fromDate = new DateTime(2018, 07, 09);
            DateTime toDate = new DateTime(2018, 07, 16);
            //get wishlist from db 
            WorkingShiftAutoTmpApi workingShiftAutoTmpApi = new WorkingShiftAutoTmpApi();
            EmployeeWorkingShiftRegitserApi employeeWorkingShiftRegitserApi = new EmployeeWorkingShiftRegitserApi();
            EmployeeWorkingShiftAutoMappingApi employeeWorkingShiftAutoMappingApi = new EmployeeWorkingShiftAutoMappingApi();
            AttendanceApi attendanceApi = new AttendanceApi();
            EmployeeApi employeeApi = new EmployeeApi();
            var listWorkShiftAuto = workingShiftAutoTmpApi.GetAll().Where(w => BetweenDate(w.Date.GetValueOrDefault(), fromDate, toDate)
                                                                            && w.IsActive == true);
            foreach (var workingShiftAuto in listWorkShiftAuto)
            {
                if (workingShiftAuto.EmployeeWorkingShiftAutoMappings != null)
                {
                    foreach (var itemMapping in workingShiftAuto.EmployeeWorkingShiftAutoMappings)
                    {
                        attendanceApi.Create(new AttendanceViewModel
                        {
                            EmployeeId = itemMapping.EmployeeId,
                            Date = workingShiftAuto.Date,
                            ShiftMin = workingShiftAuto.TimeStart,
                            ShiftMax = workingShiftAuto.TimeEnd,
                            ExpandTime = workingShiftAuto.TimeExpand,
                            Status = (int)AttendanceStatusEnum.Waiting,
                            IsActive = true,
                        });
                    }
                }
            }
        }
        public List<EmployeeWorkingShiftAutoMappingViewModel> PutEmloyeeIntoShiftYouWish(WorkingShiftAutoTmp workingShiftAuto, List<Employee> employeeInWishList, List<Employee> listEmployee)
        {
            List<EmployeeWorkingShiftAutoMappingViewModel> listEmployeeWorkingShiftAutoMappingViewModel = new List<EmployeeWorkingShiftAutoMappingViewModel>();
            EmployeeWorkingShiftAutoMappingApi employeeWorkingShiftAutoMappingApi = new EmployeeWorkingShiftAutoMappingApi();
            //xóa tất cả employee có trong wishlist ra khỏi listEmployee
            listEmployee.RemoveAll(e => e.IsActive == true && employeeInWishList.Any(x => x.Id == e.Id));
            //nhét nhân viên từ wishlist vào trong ca auto
            var countEmptySlotInWorkingShiftAuto = workingShiftAuto.EmptySlot;
            while (countEmptySlotInWorkingShiftAuto != 0 && listEmployee.Count != 0)
            {
                foreach (var emp in employeeInWishList)
                {
                    listEmployeeWorkingShiftAutoMappingViewModel.Add(new EmployeeWorkingShiftAutoMappingViewModel
                    {
                        EmployeeId = emp.Id,
                        WorkingShiftAutoTmpId = workingShiftAuto.Id,
                        IsActive = true
                    });
                    countEmptySlotInWorkingShiftAuto -= 1;
                }
                //trường hợp đã nhét hết trong wishList vào trong slot mà còn chưa đáp ứng đủ yêu cầu
                if (countEmptySlotInWorkingShiftAuto != 0)
                {
                    while (listEmployee.Count != 0 && countEmptySlotInWorkingShiftAuto != 0)
                    {
                        //random employee
                        var rand = new Random();
                        var employee = listEmployee.OrderBy(x => rand.Next()).FirstOrDefault();
                        listEmployeeWorkingShiftAutoMappingViewModel.Add(new EmployeeWorkingShiftAutoMappingViewModel
                        {
                            EmployeeId = employee.Id,
                            WorkingShiftAutoTmpId = workingShiftAuto.Id,
                            IsActive = true
                        });
                        countEmptySlotInWorkingShiftAuto -= 1;
                        listEmployee.Remove(employee);
                    }
                }
            }
            for (int i = 0; i < workingShiftAuto.EmployeeWorkingShiftAutoMappings.Count; i++)
            {
                var item = workingShiftAuto.EmployeeWorkingShiftAutoMappings.ToList()[i];

            }
            return listEmployeeWorkingShiftAutoMappingViewModel;
        }
        public static bool BetweenDate(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2 || input == date1 || input == date2);
        }
        public static bool BetweenTimeSpan(TimeSpan input, TimeSpan time1, TimeSpan time2)
        {
            return ((input > time1 && input < time2) || input == time1 || input == time2);
        }
    }
    public class WorkingShiftAutoTmpEditViewModel2
    {
        public virtual int Id { get; set; }
        public virtual String Name { get; set; }
        public virtual String Date { get; set; }
        public virtual Nullable<System.TimeSpan> TimeStart { get; set; }
        public virtual Nullable<System.TimeSpan> TimeEnd { get; set; }
        public virtual Nullable<System.TimeSpan> TimeExpand { get; set; }
        public virtual Nullable<int> EmptySlot { get; set; }
        public virtual Nullable<bool> IsActive { get; set; }

    }
}