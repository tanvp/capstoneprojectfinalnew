﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;
using Wisky.SkyAdmin.Manage.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{

    //[Authorize(Roles = Utils.AdminAuthorizeRoles)]
    public class BlogPostController : DomainBasedController
    {

        public ActionResult Index()
        {
            return this.View();
        }
        public JsonResult ChangeStatus(int blogPostId, int status)
        {
            try
            {
                var blogPostApi = new BlogPostApi();
                var blogPostSelected = blogPostApi.GetDetailBlog(blogPostId);
                if (blogPostSelected.Status != status)
                {
                    blogPostSelected.Status = status;
                    blogPostSelected.UserApprove = this.User.Identity.Name;
                    blogPostApi.BaseService.Update(blogPostSelected);
                }
                return Json(new
                {
                    success = true
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadBlogCateList(int brandId)
        {
            var blogCateapi = new BlogCategoryApi();
            var listBlogCate = blogCateapi.GetAllBlogCategoryActive();
            var parentCate = listBlogCate.Where(q => q.ParentCateId == null).ToList();
            var subCate = listBlogCate.Where(q => q.ParentCateId != null).OrderBy(q => q.ParentCateId).ToList();
            List<BlogCategory> availableBlogCateList = new List<BlogCategory>();
            foreach (var item in parentCate)
            {
                availableBlogCateList.Add(item);
                var hasSubCate = subCate.Where(q => q.ParentCateId == item.Id).ToList();
                if (hasSubCate.Count() > 0)
                {
                    availableBlogCateList.AddRange(hasSubCate);
                }

            }
            var rs = availableBlogCateList.AsQueryable().Select(q => new
            {
                q.Id,
                q.BlogCateName,
                q.ParentCateId,
            }).ToArray();
            return Json(new
            {
                success = true,
                blogCateList = rs,
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadAuthor(int brandId)
        {
            var aspNetUserApi = new AspNetUserApi();
            var listResuslt = aspNetUserApi.BaseService.Get(q => q.BrandId == brandId && !q.AspNetRoles.Any(a => a.Name == "Administrator"))
                .ToList().Select(q => new IConvertible[] { q.UserName }).ToList();
            return Json(new { listResuslt }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCollection(int brandId)
        {
            var blogPostCollectionApi = new BlogPostCollectionApi();
            var listResuslt = blogPostCollectionApi.BaseService.Get(q => q.BrandId == brandId && q.Active == true).ToList()
                .Select(q => new IConvertible[] {
                    q.Id,
                    q.Name
                }).ToList();
            return Json(new { listResuslt }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexList(JQueryDataTableParamModel param, int? blogCateId, int? collectionId, string authorName, int status, int type, string sorting, int? storeId = 0)
        {
            //,int productGroup, int status

            var blogPost = new BlogPostApi();
            string searchPattern = string.IsNullOrWhiteSpace(param.sSearch) ? null : param.sSearch;
            var result = blogPost.GetAllActiveByBlogCategoryAndPattern(blogCateId.Value, searchPattern);
            if (collectionId != 0 && collectionId != null)
            {
                result = result.Where(q => q.BlogPostCollectionItemMappings.Any(a => a.BlogPostCollectionId == collectionId && a.Active == true));
            }
            if (!String.IsNullOrEmpty(authorName))
            {
                result = result.Where(q => q.Author.ToLower().Equals(authorName.ToLower()));
            }
            if (status != -1)
            {
                result = result.Where(q => q.Status == status);
            }
            if (type != -1)
            {
                result = result.Where(q => q.BlogType == type);
            }
            if (sorting != null)
            {
                switch (sorting)
                {
                    case "CreateDesc":
                        result = result.OrderByDescending(q => q.CreatedTime);
                        break;
                    case "CreateAsc":
                        result = result.OrderBy(q => q.CreatedTime);
                        break;
                    case "UpdateDesc":
                        result = result.OrderByDescending(q => q.UpdatedTime);
                        break;
                    case "UpdateAsc":
                        result = result.OrderBy(q => q.UpdatedTime);
                        break;
                    case "PositionDSC":
                        result = result.OrderByDescending(q => q.Position);
                        break;
                    case "PositionASC":
                        result = result.OrderBy(q => q.Position);
                        break;
                    case "TotalVisitDSC":
                        result = result.OrderByDescending(q => q.Position);
                        break;
                    case "TotalVisitASC":
                        result = result.OrderBy(q => q.Position);
                        break;
                }
            }
            var allBlogCategories = new BlogCategoryApi().GetActive();
            var displayRecord = result.Count();
            var totalRecords = result.Count();
            var count = param.iDisplayStart;
            var rs = result.Skip(param.iDisplayStart)
                .Take(param.iDisplayLength).ToList()
                .Select(a => new IConvertible[]
            {
                ++count,
                a.Image,
                a.Title,
                allBlogCategories.Where(q => q.Id == a.BlogCategoryId).Any() ? allBlogCategories.Where(q => q.Id == a.BlogCategoryId).FirstOrDefault().BlogCateName: "",
                allBlogCategories.Where(q => q.Id == a.BlogCategoryId).Any() ? (allBlogCategories.Where(q => q.Id == allBlogCategories.Where(b => b.Id == a.BlogCategoryId).FirstOrDefault().Id).Any() ? allBlogCategories.Where(q => q.Id == allBlogCategories.Where(b => b.Id == a.BlogCategoryId).FirstOrDefault().Id).FirstOrDefault().BlogCateName : allBlogCategories.Where(b => b.Id == a.BlogCategoryId).FirstOrDefault().BlogCateName) : "N/A",
                a.Status,
                a.CreatedTime.ToString("hh:mm dd/MM/yyyy"),
                a.UpdatedTime.ToString("hh:mm dd/MM/yyyy"),
                a.Position == null ? 0 : a.Position,
                a.TotalVisit ?? 0,
                a.Author,
                a.LastUpdatePerson != null ? a.LastUpdatePerson : "N/A",
                a.UserApprove != null ? a.UserApprove : "N/A",
                a.Id
            }).ToList();


            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = displayRecord,
                aaData = rs
            }, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult IndexList(BootgridRequestViewModel request)
        //{
        //    var result = new BlogPostApi().GetAdminWithFilterAsync(
        //        this.CurrentStore.ID, request.searchPhrase,
        //        request.current, request.rowCount, request.FirstSortTerm);

        //    var model = new BootgridResponseViewModel<BlogPostDetailsViewModel>(result);
        //    return this.Json(model, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult UpdatePosition(int blogPostId, int position)
        {
            var blogApi = new BlogPostApi();
            var blog = blogApi.BaseService.Get(blogPostId);
            if (blog != null)
            {
                blog.Position = position;
                blogApi.BaseService.Update(blog);
                return Json(new { success = true, message = "Update Độ ưu tiên thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<ActionResult> Create()
        {
            var storeId = 0;
            if (RouteData.Values["storeId"] != null)
            {
                storeId = int.Parse(RouteData.Values["storeId"]?.ToString());
            }

            if (storeId == 0)
            {
                return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            }

            var model = new BlogPostEditViewModel()
            {
                BlogPost = new BlogPostViewModel(),
            };

            await this.PrepareCreate(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Create(BlogPostEditViewModel model, int storeId)
        {
            if (storeId == 0)
            {
                return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "HH:mm dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "HH:mm dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!this.ModelState.IsValid)
            {
                await this.PrepareCreate(model);
                return this.View(model);
            }
            var isSubmit = true;
            if (model.BlogPost.Status == 2)
            {
                isSubmit = false;
            }
            model.BlogPost.StoreId = this.CurrentStore.ID;
            model.BlogPost.BrandId = this.CurrentStore.BrandId.Value;
            model.BlogPost.Active = true;
            model.BlogPost.Author = this.User.Identity.Name;
            model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
            model.BlogPost.CreatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.Status = (int)BlogStatusEnum.Processing;
            model.BlogPost.BlogPostCollectionNumber = 0;
            var blogPostApi = new BlogPostApi();
            await blogPostApi.CreateAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0],
                model.SelectedTags ?? new int[0]);
            if (!isSubmit)
            {
                var blogId = blogPostApi.GetBlogPostIdBySeoName(model.BlogPost.SeoName, storeId);
                return this.RedirectToAction("Edit", new { id = blogId });
            }
            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        private async Task PrepareCreate(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));
            var availableBlogCate = (new BlogCategoryApi()
                .GetAllBlogCategoryActiveByBrandId(this.CurrentStore.BrandId.Value));
            var parentCate = availableBlogCate.Where(q => q.ParentCateId == null).ToList();
            var subCate = availableBlogCate.Where(q => q.ParentCateId != null).OrderBy(q => q.ParentCateId).ToList();
            List<BlogCategoryViewModel> availableBlogCateList = new List<BlogCategoryViewModel>();
            foreach (var item in parentCate)
            {
                availableBlogCateList.Add(item);
                var hasSubCate = subCate.Where(q => q.ParentCateId == item.Id).ToList();
                if(hasSubCate.Count() > 0)
                {
                    availableBlogCateList.AddRange(hasSubCate);
                }
             
            }

            var availableLocation = (new EventLocationApi().BaseService.Get(q => q.Active));
            var tags = (new TagApi().BaseService.Get(q => q.TagActive.Value));
            //var availableBlogPost = (new BlogPostApi().BaseService.Get(q => q.Active && q.Status == (int)BlogStatusEnum.Approve).OrderByDescending(q => q.UpdatedTime));
            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);
            //model.AvailableBlogCategories = availableBlogCateList.AsQueryable().ToSelectList(q => q.BlogCateName, q => q.Id.ToString(), q => false);
            model.AvailableBlogCategories = availableBlogCateList.AsQueryable();
            model.AvailableLocation = availableLocation.ToSelectList(q => q.LocationName, q => q.LocationId.ToString(), q => false);
            //model.AvailableBlogReference = availableBlogPost.ToSelectList(q => q.PageTitle, q => q.Id.ToString(), q => false);
            model.Tags = tags.ToList();
            model.SelectedTags = new int[0];
        }

        public async Task<ActionResult> Edit(int? id)
        {
            var info = new BlogPostApi()
                .GetDetailsByStoreId(id.GetValueOrDefault(), this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }

            var model = new BlogPostEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogPostEditViewModel model, int storeId)
        {
            var api = new BlogPostApi();
            // Validate
            var info =  api
                .GetByStoreAsyn(model.BlogPost.Id, this.CurrentStore.ID);

            BlogPostCollectionApi blogPostCollectionApi = new BlogPostCollectionApi();
            var selectedCollections = blogPostCollectionApi.GetSelectedCollection(model.SelectedBlogPostCollections);
            int collectionNumber = CalculateCollectionNumber(selectedCollections, model.BlogPost.Id);

            if (info == null)
            {
                return this.IdNotFound();
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "HH:mm dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(model.StartDate))
            {
                model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "HH:mm dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (!string.IsNullOrWhiteSpace(model.PublicDate))
            {
                model.BlogPost.PublicDate = DateTime.ParseExact(model.PublicDate, "HH:mm dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (!this.ModelState.IsValid)
            {
                await this.PrepareEdit(model);
                return this.View(model);
            }
            var isSubmit = true;
            if (model.BlogPost.Status == 2)
            {
                isSubmit = false;
            }
            //model.BlogPost.Status = info.Status;
            if (info.Status == (int)BlogStatusEnum.Processing)
            {
                model.BlogPost.UserApprove = null;
            }

            model.BlogPost.BlogPostCollectionNumber = collectionNumber;
            model.BlogPost.StoreId = info.StoreId;
            model.BlogPost.BrandId = info.BrandId;
            model.BlogPost.CreatedTime = info.CreatedTime;
            model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            model.BlogPost.Active = info.Active;
            model.BlogPost.Author = info.Author;
            model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
            if (!isSubmit)
            {
                model.BlogPost.Status = (int)BlogStatusEnum.Processing;
            }
            else
            {
                if (storeId == 0)
                {
                    model.BlogPost.Status = (int)BlogStatusEnum.Approve;
                }
                else
                {
                    model.BlogPost.Status = (int)BlogStatusEnum.Processing;
                }
            }
            //model.BlogPost.Status = (int)BlogStatusEnum.Processing;
            if (model.PublicDate != null)
            {
                model.BlogPost.Status = (int)BlogStatusEnum.Scheduled;
            }
            //if (model.PublicDate == null)
            //{
            //    model.BlogPost.Status = (int)BlogStatusEnum.Processing;
            //}
            model.BlogPost.AuthorRefer = info.AuthorRefer;
            await api.EditAsync(model.BlogPost,
                model.SelectedBlogPostCollections ?? new int[0],
                model.SelectedImages ?? new string[0],
                model.SelectedTags ?? new int[0]);
            if (!isSubmit)
            {
                var blogId = api.GetBlogPostIdBySeoName(model.BlogPost.SeoName, model.BlogPost.StoreId);
                return this.RedirectToAction("Edit", new { id = blogId });
            }
            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }

        public async Task PrepareEdit(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));
            var availableBlogCate = (new BlogCategoryApi()
                .GetAllBlogCategoryActiveByBrandId(this.CurrentStore.BrandId.Value));
            var availableLocation = (new EventLocationApi().BaseService.Get(q => q.Active));
            var tags = (new TagApi().BaseService.GetActive());
            //var availableBlogPost = (new BlogPostApi().BaseService.Get(q => q.Active && q.Status == (int)BlogStatusEnum.Approve).OrderByDescending(q => q.UpdatedTime));
            var parentCate = availableBlogCate.Where(q => q.ParentCateId == null).ToList();
            var subCate = availableBlogCate.Where(q => q.ParentCateId != null).OrderBy(q => q.ParentCateId).ToList();
            List<BlogCategoryViewModel> availableBlogCateList = new List<BlogCategoryViewModel>();
            foreach (var item in parentCate)
            {
                availableBlogCateList.Add(item);
                var hasSubCate = subCate.Where(q => q.ParentCateId == item.Id).ToList();
                if (hasSubCate.Count() > 0)
                {
                    availableBlogCateList.AddRange(hasSubCate);
                }

            }

            var seletedTags = new TagMappingApi().GetAllMappingByBlogId(model.BlogPost.Id).Select(q => q.TagId);
            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedBlogPostCollections.Contains(q.Id));
            //model.AvailableBlogCategories = availableBlogCate.ToSelectList(q => q.BlogCateName, q => q.Id.ToString(), q => q.Id == model.BlogPost.BlogCategoryId);
            model.AvailableBlogCategories = availableBlogCateList;
            model.AvailableLocation = availableLocation.ToSelectList(q => q.LocationName, q => q.LocationId.ToString(), q => q.LocationId == model.BlogPost.LocationId);
            //model.AvailableBlogReference = availableBlogPost.Select(q => new SelectListItem { Value = q.Id.ToString(), Text = q.Title });
            model.Tags = tags.ToList();
            model.SelectedTags = seletedTags.ToArray<int>();

        }

        [HttpPost]
        public async Task<ActionResult> Delete(int? id, int? storeId = 0)
        {
            if (storeId != 0)
            {
                return Json(new { }, JsonRequestBehavior.DenyGet);
            }
            var productApi = new BlogPostApi();
            var blogPostApi = new BlogPostApi();
            //var info = await productApi
            //    .GetByStoreIdAsync(id.GetValueOrDefault(), this.CurrentStore.ID);

            //if (info == null)
            //{
            //    return this.IdNotFound();
            //}

            await productApi.DeactivateAsync(id.Value);
            if(id != null)
            {
                await blogPostApi.EditCollectionNumberAsync(id.Value, 0);
            }

            return Json(new { message = "Delete" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckSeoName(string seoName, int blogPostId)
        {
            var blogPostApi = new BlogPostApi();
            var rs = blogPostApi.BaseService.Get(q => q.SeoName.Trim() == seoName && (q.Id != blogPostId));
            if (rs.FirstOrDefault() != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        //Calculating collectionNumber and adding CollectionItem
        public int CalculateCollectionNumber(List<BlogPostCollection> collections, int blogPostId)
        {
            int total = 0;
            if (collections != null)
            {
                foreach (var item in collections)
                {
                    total += (int)Math.Pow(2, (int)item.Position);
                    AddMappingItem(item.Id, blogPostId);
                }
            }
            return total;
        }

        public void AddMappingItem(int blogPostCollectionId, int blogPostId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionItemMappingApi = new BlogPostCollectionItemMappingApi();
            BlogPostCollectionItemMappingViewModel blogPostCollectionItemMappingViewModel;
            blogPostCollectionItemMappingViewModel = new BlogPostCollectionItemMappingViewModel
            {
                BlogPostCollectionId = blogPostCollectionId,
                BlogPostId = blogPostId,
                Position = 0,
                Active = true,
            };

            if(!CheckExistMappingItem(blogPostCollectionId, blogPostId))
                blogPostCollectionItemMappingApi.Create(blogPostCollectionItemMappingViewModel);
        }

        public bool CheckExistMappingItem(int blogPostCollectionId, int blogPostId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionItemMappingApi = new BlogPostCollectionItemMappingApi();
            BlogPostCollectionItemMapping blogPostCollectionItemMapping = blogPostCollectionItemMappingApi.GetBlogPostMappingInCollection(blogPostCollectionId, blogPostId);
            if(blogPostCollectionItemMapping != null)
            {
                return true;
            }
            return false;
        }
    }
}