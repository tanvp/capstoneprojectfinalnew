﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class BlogPostCollectionItemMappingController : DomainBasedController
    {
        public bool Edit(int mappingId, int newPosition)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                return false;
            }
            try
            {
                var blogPostCollectionItemMappingApi = new BlogPostCollectionItemMappingApi();
                bool res = blogPostCollectionItemMappingApi.EditBlogPostPositionInColelction(mappingId, newPosition);
                if (res)
                {
                    Response.StatusCode = 200;
                    return true;
                }
                else
                {
                    Response.StatusCode = 400;
                    return false;
                }
            }
            catch (Exception)
            {
                Response.StatusCode = 400;
                return false;
                throw;
            }
        }

        public ActionResult Remove(int mappingId)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                return Json("Id không đúng");
            }
            try
            {
                var blogPostCollectionItemMappingApi = new BlogPostCollectionItemMappingApi();
                var blogPostCollectionItemMapping = blogPostCollectionItemMappingApi.GetBlogPostMappingByMappingId(mappingId);

                bool isMinuted = MinuteCollectionNumber(blogPostCollectionItemMapping.BlogPost, blogPostCollectionItemMapping.BlogPostCollection);
                bool res = false;
                if (isMinuted)
                {
                    res = blogPostCollectionItemMappingApi.RemoveBlogPostFromCollection(mappingId);
                }

                if (res)
                {
                    Response.StatusCode = 200;
                    return Json("Thành công");
                }
                else
                {
                    Response.StatusCode = 400;
                    return Json("Có lỗi xảy ra");
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Json("Có lỗi xảy ra");
                throw;
            }
        }

        public ActionResult Index(int collectionId)
        {
            var blogPostCollectionApi = new BlogPostCollectionApi();
            BlogPostCollectionViewModel blogPostCollection = blogPostCollectionApi.Get(collectionId);
            ViewBag.CollectionName = blogPostCollection.Name;
            ViewBag.MaxShow = blogPostCollection.Limitation;
            ViewBag.CollectionId = collectionId;
            return View();
        }

        public JsonResult IndexList(JQueryDataTableParamModel param, int collectionId)
        {
            var blogPostCollectionItemMappingApi = new BlogPostCollectionItemMappingApi();
            string searchPattern = string.IsNullOrWhiteSpace(param.sSearch) ? null : param.sSearch;
            List<BlogPostCollectionItemMapping> result;

            if (param.sSearch != null)
            {
                result = blogPostCollectionItemMappingApi.GetBlogPostMappingByCollectionAndSearch(collectionId, param.sSearch);
            }
            else
            {
                result = blogPostCollectionItemMappingApi.GetBlogPostMappingByCollection(collectionId);
            }

            var displayRecord = result.Count();
            var totalRecords = result.Count();
            var count = param.iDisplayStart;
            var rs = result.Skip(param.iDisplayStart)
                .Take(param.iDisplayLength).ToList()
                .Select(a => new IConvertible[]
            {
                ++count,
                a.BlogPost.Title,
                a.Position,
                a.Id
            }).ToList();

            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = displayRecord,
                aaData = rs
            }, JsonRequestBehavior.AllowGet);
        }

        public bool MinuteCollectionNumber(BlogPost blogPost, BlogPostCollection blogPostCollection)
        {
            var blogPostApi = new BlogPostApi();
            try
            {
                if (blogPost != null)
                {
                    int collectionNumer = ((int)blogPost.BlogPostCollectionNumber - (int)Math.Pow(2, (int)blogPostCollection.Position));
                    blogPostApi.EditCollectionNumber(blogPost.Id, collectionNumer);
                    return true;
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return false;
        }
    }
}