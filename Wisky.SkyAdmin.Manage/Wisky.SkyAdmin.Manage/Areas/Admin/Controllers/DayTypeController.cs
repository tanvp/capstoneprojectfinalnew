﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;
using HmsService.Models;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class DayTypeController : Controller
    {
        // GET: DayType
        public ActionResult Index(int brandId)
        {
            var dayTypeEditViewModel = new DayTypeEditViewModel();
            dayTypeEditViewModel.WeekDayNames = loadEnum();
            return View("Index", dayTypeEditViewModel);
        }
        public ActionResult Create()
        {
            var dayTypeEditViewModel = new DayTypeEditViewModel();
            dayTypeEditViewModel.WeekDayNames = loadEnum();
            return View("Create", dayTypeEditViewModel);
        }
        public ActionResult Edit()
        {
            return View();
        }

        public JsonResult GetDayType(JQueryDataTableParamModel param)
        {
            try
            {
                var dayTypeApi = new DayTypeApi();
                var dayTypeList = dayTypeApi.GetAll().ToList();

                var count = 0;
                var list = dayTypeList.Select(dayType => new object[]
                {
                    ++count,
                    dayType.Id,
                    dayType.DayTypeName != null ? dayType.DayTypeName: "",
                    dayType.DaysOfTheWeek != null? dayType.DaysOfTheWeek: "",
                    dayType.FromDate != null ? dayType.FromDate.GetValueOrDefault().ToString("dd/MM/yyyy"): "",
                    dayType.ToDate != null ? dayType.ToDate.GetValueOrDefault().ToString("dd/MM/yyyy"): ""
                }).ToList();

                var totalRecord = list.Count();
                return Json(new { sEcho = param.sEcho, iTotalRecords = totalRecord, iTotalDisplayRecords = totalRecord, aaData = list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpPost]
        public async Task<JsonResult> EditDayTypeAsync(String id, String dayTypeName, String daysOfWeek, String dayTypeRate, String fromDate, String toDate)
        {
            bool result = false;
            try
            {
                var dayTypeApi = new DayTypeApi();
                var dayType = await dayTypeApi.GetAsync(Int32.Parse(id));
                dayType.DayTypeName = dayTypeName;
                dayType.DaysOfTheWeek = daysOfWeek;
                if (fromDate != "")
                {
                    var tmpDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
                    dayType.FromDate = tmpDate;
                }
                if (toDate != "")
                {
                    var tmpDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);
                    dayType.ToDate = tmpDate;
                }
                await dayTypeApi.EditAsync(Int32.Parse(id), dayType);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return Json(result);
        }
        [HttpPost]
        public async Task<JsonResult> DeleteDayTypeAsync(String id)
        {
            string[] result = new string[2] { "failed", "failed" };
            try
            {
                var timeModeApi = new TimeModeApi();
                var timeModeList = timeModeApi.GetAll().Where(t => t.DayTypeId == Int32.Parse(id));
                if (timeModeList.Any())
                {
                    result[0] = timeModeList.First().TimeModeName;
                }
                else
                {
                    result[0] = "";
                    var dayTypeApi = new DayTypeApi();
                    var dayType = await dayTypeApi.GetAsync(Int32.Parse(id));
                    if (dayType.IsActive)
                    {
                        dayType.IsActive = false;
                        await dayTypeApi.EditAsync(Int32.Parse(id), dayType);
                        result[1] = "";
                    }
                    else
                    {
                        result[1] = "failed";
                    }
                }
            }
            catch (Exception ex)
            {
                result = new string[2] { "", "failed" };
            }
            return Json(result);
        }
        [HttpPost]
        public async Task<ActionResult> CreateDayTypeAsync(DayTypeEditViewModel editModel)
        {
            try
            {
                editModel.DayTypeViewModel.BrandId = 1;
                editModel.DayTypeViewModel.IsActive = true;
                var dayTypeApi = new DayTypeApi();
                await dayTypeApi.CreateAsync(editModel.DayTypeViewModel);
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Index");
        }
        private string[] loadEnum()
        {
            string[] nameList = new string[]
            {
                DaysOfWeekEnum.Monday.DisplayName(),
                DaysOfWeekEnum.Tuesday.DisplayName(),
                DaysOfWeekEnum.Wednesday.DisplayName(),
                DaysOfWeekEnum.Thursday.DisplayName(),
                DaysOfWeekEnum.Friday.DisplayName(),
                DaysOfWeekEnum.Saturday.DisplayName(),
                DaysOfWeekEnum.Sunday.DisplayName()
            };
            return nameList;
        }
    }
}