﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class AdsRegisterController : DomainBasedController
    {
        // GET: Admin/AdsRegister
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult IndexList(JQueryDataTableParamModel param, int brandId)
        //{
        //    var adsRegisterApi = new AdsRegisterApi();
        //    var allBlogCate = adsRegisterApi.GetAllAdsRegister();
        //    var totalRecords = allBlogCate.Count();
        //    if (!String.IsNullOrEmpty(param.sSearch))
        //    {
        //        allBlogCate = allBlogCate.Where(q => q.Company.ToLower().Equals(param.sSearch.ToLower())).ToList();
        //    }
        //    var totalDisplay = allBlogCate.Count();
        //    var count = 1;
        //    var result = allBlogCate.OrderByDescending(q => q.Position).Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList().Select(q => new IConvertible[] {
        //        count++,
        //        q.NameAds,
        //        q.Company,
        //        q.ImageUrl,
        //        q.Content,
        //        q.Click,
        //        q.Position,
        //        q.isActive,
        //        q.Id
        //    });

        //    return Json(new
        //    {
        //        sEcho = param.sEcho,
        //        iTotalRecords = totalRecords,
        //        iTotalDisplayRecords = totalDisplay,
        //        aaData = result
        //    }, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult IndexList(JQueryDataTableParamModel param, int brandId)
        //{
        //    var allAds = new BlogPostApi().GetAllBlogPostByTypeForAds((int)BlogTypeEnum.Adsvertisement, brandId);
            
        //    var totalRecords = allAds.Count();
        //    if (!String.IsNullOrEmpty(param.sSearch))
        //    {
        //        allAds = allAds.Where(q => q.Title.ToLower().Equals(param.sSearch.ToLower())).ToList();
        //    }
        //    var totalDisplay = allAds.Count();
        //    var count = 1;
        //    var result = allAds/*.OrderByDescending(q => q.Position)*/.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList().Select(q => new IConvertible[] {
        //        count++,
        //        q.Title,
        //        q.Title_En,
        //        q.Image,
        //        q.BlogContent,
        //        q.TotalVisit,
        //        q.Position,
        //        q.Status,
        //        q.Id
        //    });

        //    return Json(new
        //    {
        //        sEcho = param.sEcho,
        //        iTotalRecords = totalRecords,
        //        iTotalDisplayRecords = totalDisplay,
        //        aaData = result
        //    }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetAllCompanies(int brandId)
        {
            var adsRegisterApi = new AdsRegisterApi();
            var listCompanies = adsRegisterApi.GetAllCompanies(brandId).ToList();
            return Json(new { data = listCompanies }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Create(int brandId)
        //{
        //    var model = new AdsRegisterViewModel();
        //    model.BrandID = brandId;
        //    model.UserName = System.Web.HttpContext.Current.User.Identity.Name;
        //    model.StartDate = DateTime.Today;
        //    model.EndDate = DateTime.Today.AddDays(1);
        //    model.LastUpdatePerson = System.Web.HttpContext.Current.User.Identity.Name;
        //    model.Position = 0;
        //    return View(model);
        //}

        public async Task<ActionResult> Create(int brandId)
        {
            //var storeId = 0;
            //if (RouteData.Values["storeId"] != null)
            //{
            //    storeId = int.Parse(RouteData.Values["storeId"]?.ToString());
            //}

            //if (storeId == 0 && brandId == 0)
            //{
            //    return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            //}

            //var model = new BlogPostEditViewModel()
            //{
            //    BlogPost = new BlogPostViewModel(),
            //};

            //model.BlogPost.BrandId = brandId;
            //model.BlogPost.Author = System.Web.HttpContext.Current.User.Identity.Name;
            //model.BlogPost.CreatedTime = DateTime.Today;
            //model.BlogPost.StartDate = DateTime.Today;
            //model.BlogPost.EndDate = DateTime.Today.AddDays(1);
            //model.BlogPost.LastUpdatePerson = System.Web.HttpContext.Current.User.Identity.Name;
            //model.BlogPost.Position = 0;
            //model.BlogPost.TotalVisit = 0;
            //model.BlogPost.BlogType = (int)BlogTypeEnum.Adsvertisement;

            //await this.PrepareCreate(model);
            return this.View();
        }

        [HttpPost, ValidateInput(false)]
        //public async Task<ActionResult> Create(BlogPostEditViewModel model, int storeId, int brandId)
        //{
        //    if (storeId == 0 && brandId == 0)
        //    {
        //        return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        //    }
        //    if (!string.IsNullOrWhiteSpace(model.StartDate))
        //    {
        //        model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //    }
        //    if (!string.IsNullOrWhiteSpace(model.EndDate))
        //    {
        //        model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //    }
        //    if (!this.ModelState.IsValid)
        //    {
        //        await this.PrepareCreate(model);
        //        return this.View(model);
        //    }
        //    BlogCategoryApi blogCategoryApi = new BlogCategoryApi();

        //    model.BlogPost.StoreId = this.CurrentStore.ID;
        //    model.BlogPost.BrandId = this.CurrentStore.BrandId.Value;
        //    model.BlogPost.Active = true;
        //    model.BlogPost.Author = this.User.Identity.Name;
        //    model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
        //    model.BlogPost.CreatedTime = Utils.GetCurrentDateTime();
        //    model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
        //    model.BlogPost.BlogType = (int)BlogTypeEnum.Adsvertisement;
        //    if (model.BlogPost.Active)
        //    {
        //        model.BlogPost.Status = (int)AdsStatusEnum.IsActive;
        //    }
        //    else
        //    {
        //        model.BlogPost.Status = (int)AdsStatusEnum.NotActive;
        //    }
        //    model.BlogPost.Active = false;
        //    model.BlogPost.SeoName = "ads";
        //    //model.BlogPost.Status = (int)AdsStatusEnum.Approve;
        //    model.BlogPost.BlogPostCollectionNumber = 0;
        //    //var blogCategory = blogCategoryApi.GetIdCateByType((int)BlogCateTypeEnum.News, brandId).FirstOrDefault();// for save(entity)
        //    //model.BlogPost.BlogCategoryId = blogCategory;

        //    if (model.SelectedImages == null)
        //    {
        //        string[] arr = { model.BlogPost.Image };
        //        model.SelectedImages = arr;
        //    }
        //    var blogPostApi = new BlogPostApi();
        //    await blogPostApi.CreateAsync(model.BlogPost,
        //        model.SelectedBlogPostCollections ?? new int[0],
        //        model.SelectedImages ?? new string[0],
        //        model.SelectedTags ?? new int[0]);

        //    return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        //}

        private async Task PrepareCreate(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));
            
            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => false);
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="brandId"></param>
        /// <returns></returns>
        //[HttpPost, ValidateInput(false)]
        //public ActionResult Create(AdsRegisterViewModel model, int brandId)
        //{
        //    if (model != null)
        //    {
        //        var entity = model.ToEntity();
        //        var adsRegisterApi = new AdsRegisterApi();
        //        try
        //        {
        //            var existAdsName = adsRegisterApi.Get().Where(b => b.NameAds == entity.NameAds).FirstOrDefault();
        //            model.StartDate = Utils.ToDateTime(model.stringDateStart);
        //            model.EndDate = Utils.ToDateTime(model.stringDateEnd);
        //            if (existAdsName != null)
        //            {
        //                List<string> notification = new List<string>();
        //                notification.Add("Tên quảng cáo đã tồn tại");
        //                ViewData["Notification"] = notification;
        //                return View(model);
        //            }
        //            if (model.stringDateStart == null || model.stringDateEnd == null)
        //            {
        //                List<string> notification = new List<string>();
        //                notification.Add("Ngày bắt đầu và ngày kết thúc không phù hợp");
        //                ViewData["Notification"] = notification;
        //                return View(model);
        //            }
        //            entity.StartDate = Utils.ToDateTime(model.stringDateStart);
        //            entity.EndDate = Utils.ToDateTime(model.stringDateEnd);
        //            //if (entity.StartDate < DateTime.Today.AddDays(-1))
        //            //{
        //            //    List<string> notification = new List<string>();
        //            //    notification.Add("Ngày bắt đầu đã trôi qua");
        //            //    ViewData["Notification"] = notification;
        //            //    return View(model);
        //            //}
        //            if (entity.StartDate > entity.EndDate)
        //            {
        //                List<string> notification = new List<string>();
        //                notification.Add("Ngày bắt đầu phải trước ngày kết thúc");
        //                ViewData["Notification"] = notification;
        //                return View(model);
        //            }
        //            if (model.isActive == false || model.isActive == null)
        //            {
        //                entity.isActive = false;
        //            }
        //            if(model == null)
        //            {
        //                entity.isActive = true;
        //            }

        //            entity.BrandID = brandId;
        //            entity.Click = 0;
        //            entity.Company = model.Company;
        //            entity.Content = model.Content;
        //            entity.Description = model.Description;
        //            entity.CreateDate = DateTime.Today;
        //            entity.ImageUrl = model.ImageUrl;
        //            entity.isActive = model.isActive;
        //            entity.LastUpdate = DateTime.Today;
        //            entity.LastUpdatePerson = System.Web.HttpContext.Current.User.Identity.Name;
        //            entity.NameAds = model.NameAds;
        //            entity.UserName = System.Web.HttpContext.Current.User.Identity.Name;
        //            entity.UrlRedirect = model.UrlRedirect;
        //            entity.Position = model.Position;

        //            adsRegisterApi.BaseService.Create(entity);
        //            return View("Index");
        //        }
        //        catch (Exception ex)
        //        {
        //            return View(model);
        //        }

        //    }
        //    else
        //    {
        //        return View(model);
        //    }

        //}

        //public ActionResult Delete(int brandId, int id)
        //{
        //    var adsRegisterApi = new AdsRegisterApi();
        //    var adsRegister = adsRegisterApi.BaseService.Get(id);
        //    if (adsRegister != null)
        //    {
        //        adsRegister.isActive = false;
        //        try
        //        {
        //            adsRegisterApi.BaseService.Update(adsRegister);
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(new { success = false, message = "Có lỗi xảy ra trong quá trình xử lý, vui lòng thử lại" });
        //        }
        //    }
        //    else
        //    {
        //        return Json(new { success = false, message = "Không Tồn Tại Chuyên Mục Này" });
        //    }
        //    return Json(new { success = true, message = "Xóa thành công" });
        //}

        public ActionResult Delete(int brandId, int id)
        {
            //BlogPostApi blogPostApi = new BlogPostApi();
            //BlogPost adsBlog = blogPostApi.GetBlogPostById(id);
            //if (adsBlog != null)
            //{
            //    adsBlog.BlogPostCollectionNumber = 0;
            //    adsBlog.BlogType = 0;// is removed when not matched enum
            //    try
            //    {
            //        blogPostApi.BaseService.Update(adsBlog);
            //    }
            //    catch (Exception ex)
            //    {
            //        return Json(new { success = false, message = "Có lỗi xảy ra trong quá trình xử lý, vui lòng thử lại" });
            //    }
            //}
            //else
            //{
            //    return Json(new { success = false, message = "Không Tồn Tại Chuyên Mục Này" });
            //}
            //return Json(new { success = true, message = "Xóa thành công" });
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">cateId</param>
        /// <returns></returns>
        //public ActionResult Edit(int id)
        //{
        //    var adsRegisterApi = new AdsRegisterApi();
        //    var adsRegister = adsRegisterApi.GetAdsRegisterById(id);
        //    var adsRegisterVM = new AdsRegisterViewModel();

        //    adsRegisterVM.Id = adsRegister.Id;
        //    adsRegisterVM.BrandID = adsRegister.BrandID;
        //    adsRegisterVM.Click = adsRegister.Click;
        //    adsRegisterVM.Company = adsRegister.Company;
        //    adsRegisterVM.Content = adsRegister.Content;
        //    adsRegisterVM.CreateDate = adsRegister.CreateDate;
        //    adsRegisterVM.Description = adsRegister.Description;
        //    adsRegisterVM.ImageUrl = adsRegister.ImageUrl;
        //    adsRegisterVM.UserName = adsRegister.UserName;
        //    adsRegisterVM.isActive = adsRegister.isActive;
        //    adsRegisterVM.LastUpdate = adsRegister.LastUpdate;
        //    adsRegisterVM.LastUpdatePerson = adsRegister.LastUpdatePerson;
        //    adsRegisterVM.NameAds = adsRegister.NameAds;
        //    adsRegisterVM.StartDate = adsRegister.StartDate;
        //    adsRegisterVM.EndDate = adsRegister.EndDate;
        //    adsRegisterVM.UrlRedirect = adsRegister.UrlRedirect;
        //    adsRegisterVM.Position = adsRegister.Position;

        //    //ViewBag.AdsRegister = adsRegisterVM;
        //    return View(adsRegisterVM);
        //}

        public async Task<ActionResult> Edit(int id)
        {
            var info = new BlogPostApi()
                .GetDetailsByStoreId(id, this.CurrentStore.ID);

            if (info == null)
            {
                return this.IdNotFound();
            }
           
            var model = new BlogPostEditViewModel(info, this.Mapper);

            await this.PrepareEdit(model);
            return this.View(model);
        }

        //[HttpPost, ValidateInput(false)]
        //public ActionResult Edit(AdsRegisterViewModel adsRegisterVM)
        //{
        //    if (adsRegisterVM != null)
        //    {
        //        var adsRegisterApi = new AdsRegisterApi();
        //        try
        //        {
        //            if (adsRegisterVM.isActive == false || adsRegisterVM.isActive == null)
        //            {
        //                adsRegisterVM.isActive = false;
        //            }
        //            if (adsRegisterVM == null)
        //            {
        //                adsRegisterVM.isActive = true;
        //            }
        //            adsRegisterVM.StartDate = DateTime.Today;
        //            adsRegisterVM.EndDate = DateTime.Today.AddDays(1);

        //            AdsRegister adsRegister = adsRegisterApi.GetAdsRegisterById(adsRegisterVM.Id);
        //            var existAdsName = adsRegisterApi.Get().Where(b => b.NameAds == adsRegisterVM.NameAds).FirstOrDefault();
        //            if (existAdsName != null)
        //            {
        //                if (existAdsName.Id != existAdsName.Id)
        //                {
        //                    List<string> notification = new List<string>();
        //                    notification.Add("Tên quảng cáo đã tồn tại");
        //                    ViewData["Notification"] = notification;
        //                    return View(adsRegisterVM);
        //                }
        //            }
        //            if (adsRegisterVM.stringDateStart == null || adsRegisterVM.stringDateEnd == null)
        //            {
        //                List<string> notification = new List<string>();
        //                notification.Add("Ngày bắt đầu và ngày kết thúc không phù hợp");
        //                ViewData["Notification"] = notification;
        //                return View(adsRegisterVM);
        //            }
        //            adsRegisterVM.StartDate = Utils.ToDateTime(adsRegisterVM.stringDateStart);
        //            adsRegisterVM.EndDate = Utils.ToDateTime(adsRegisterVM.stringDateEnd);
        //            adsRegister.StartDate = Utils.ToDateTime(adsRegisterVM.stringDateStart);
        //            adsRegister.EndDate = Utils.ToDateTime(adsRegisterVM.stringDateEnd);
        //            //if (adsRegister.StartDate < DateTime.Today.AddDays(-1))
        //            //{
        //            //    List<string> notification = new List<string>();
        //            //    notification.Add("Ngày bắt đầu đã trôi qua");
        //            //    ViewData["Notification"] = notification;
        //            //    return View(adsRegisterVM);
        //            //}
        //            if (adsRegister.StartDate > adsRegister.EndDate)
        //            {
        //                List<string> notification = new List<string>();
        //                notification.Add("Ngày bắt đầu phải trước ngày kết thúc");
        //                ViewData["Notification"] = notification;
        //                return View(adsRegisterVM);
        //            }
        //            adsRegister.ImageUrl = adsRegisterVM.ImageUrl;
        //            adsRegister.NameAds = adsRegisterVM.NameAds;
        //            adsRegister.StartDate = Utils.ToDateTime(adsRegisterVM.stringDateStart);
        //            adsRegister.EndDate = Utils.ToDateTime(adsRegisterVM.stringDateEnd);
        //            adsRegister.Company = adsRegisterVM.Company;
        //            adsRegister.Content = adsRegisterVM.Content;
        //            adsRegister.isActive = adsRegisterVM.isActive;
        //            adsRegister.LastUpdate = DateTime.Today;
        //            adsRegister.LastUpdatePerson = System.Web.HttpContext.Current.User.Identity.Name;
        //            adsRegister.UrlRedirect = adsRegisterVM.UrlRedirect;
        //            adsRegister.Position = adsRegisterVM.Position;

        //            adsRegisterApi.BaseService.Update(adsRegister);
        //            adsRegisterVM.StartDate = adsRegister.StartDate;
        //            adsRegisterVM.EndDate = adsRegister.EndDate;
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception ex)
        //        {
        //            List<string> notification = new List<string>();
        //            notification.Add("Có lỗi xảy ra! Không thể hoàn thành cập nhật!");
        //            ViewData["Notification"] = notification;
        //            return View(adsRegisterVM);
        //        }

        //    }
        //    else
        //    {
        //        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        //    }
        //}


        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> Edit(BlogPostEditViewModel model)
        {
            //var api = new BlogPostApi();
            //// Validate
            //var info = api
            //    .GetByStoreAsyn(model.BlogPost.Id, this.CurrentStore.ID);


            //BlogPostCollectionApi blogPostCollectionApi = new BlogPostCollectionApi();
            ////var selectedCollections = blogPostCollectionApi.GetSelectedCollection(model.SelectedBlogPostCollections);
            //List<BlogPostCollection> selectedCollections = null;
            //int collectionNumber = 0;
            //if (model.SelectedBlogPostCollections != null)
            //{
            //    selectedCollections = blogPostCollectionApi.GetSelectedCollection(model.SelectedBlogPostCollections);
            //    collectionNumber = CalculateCollectionNumber(selectedCollections);
            //}


            //if (info == null)
            //{
            //    return this.IdNotFound();
            //}
            //if (!string.IsNullOrWhiteSpace(model.StartDate))
            //{
            //    model.BlogPost.StartDate = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //}
            //if (!string.IsNullOrWhiteSpace(model.StartDate))
            //{
            //    model.BlogPost.EndDate = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //}

            //if (!this.ModelState.IsValid && model.SelectedBlogPostCollections != null)
            //{
            //    await this.PrepareEdit(model);
            //    return this.View(model);
            //}
            //model.BlogPost.Status = info.Status;
            //if (model.BlogPost.Active)
            //{
            //    model.BlogPost.Status = (int)AdsStatusEnum.IsActive;
            //}
            //else
            //{
            //    model.BlogPost.Status = (int)AdsStatusEnum.NotActive;
            //}
            ////if (info.Status == (int)BlogStatusEnum.Processing)
            ////{
            ////    model.BlogPost.UserApprove = null;
            ////}
            //model.BlogPost.BlogPostCollectionNumber = collectionNumber;
            //model.BlogPost.StoreId = info.StoreId;
            //model.BlogPost.BrandId = info.BrandId;
            //model.BlogPost.CreatedTime = info.CreatedTime;
            //model.BlogPost.UpdatedTime = Utils.GetCurrentDateTime();
            //model.BlogPost.Active = info.Active;
            //model.BlogPost.Author = info.Author;
            //model.BlogPost.LastUpdatePerson = this.User.Identity.Name;
            //model.BlogPost.AuthorRefer = info.AuthorRefer;
            //model.BlogPost.BlogType = (int)BlogTypeEnum.Adsvertisement;

            //if (model.SelectedImages == null)
            //{
            //    string[] arr = { model.BlogPost.Image };
            //    model.SelectedImages = arr;
            //}
            //await api.EditAsync(model.BlogPost,
            //    model.SelectedBlogPostCollections ?? new int[0],
            //    model.SelectedImages ?? new string[0],
            //    model.SelectedTags ?? new int[0]);

            //return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
            return null;
        }

        public async Task PrepareEdit(BlogPostEditViewModel model)
        {
            var availableCollection = (await new BlogPostCollectionApi()
                .GetActiveByStoreIdAsync(this.CurrentStore.BrandId.Value));

            model.AvailableCollections = availableCollection.ToSelectList(q => q.Name, q => q.Id.ToString(), q => model.SelectedBlogPostCollections.Contains(q.Id));

        }

        //public int CalculateCollectionNumber(List<BlogPostCollection> collections)
        //{
        //    int total = 0;
        //    foreach (var item in collections)
        //    {
        //        total += (int)Math.Pow(2, (int)item.Position);
        //    }
        //    return total;
        //}
    }
}