﻿using HmsService.Models;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class AttendanceController : Controller
    {
        // GET: Attendance
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Job()
        {
            return View();
        }
        public ActionResult Job2()
        {
            return View();
        }
        public ActionResult detailsAttendanceWeek(int storeId, string Date, string startTime, string endTime)
        {
            var startTimeParse = Utils.ToDateTimeHour(startTime);

            var endTimeParse = startTimeParse.Add(new TimeSpan(0, 59, 59));

            int count = 1;
            var apiAttendance = new AttendanceApi();
            var employeeApi = new EmployeeApi();

            var startDate = Date.ToDateTime().GetStartOfDate();
            var endDate = Date.ToDateTime().GetEndOfDate();
            var datatable = apiAttendance.GetAttendanceByTimeRangeAndStore2(storeId, startDate, endDate).Where(q => !(q.ShiftMin > endTimeParse.TimeOfDay && q.ShiftMin > startTimeParse.TimeOfDay) && !(startTimeParse.TimeOfDay > q.ShiftMin && startTimeParse.TimeOfDay > q.ShiftMax) && q.Date == startDate.Date).ToList();
            var storeApi = new StoreApi();
            var result = datatable.OrderBy(q => q.ShiftMin).Select(q => new IConvertible[]
              {
                count++,
                employeeApi.Get(q.EmployeeId).EmployeeName,
                q.ShiftMin.Value.ToString(),
                q.ShiftMax.Value.ToString(),
                q.CheckMin == null ? "" : q.CheckMin.Value.ToString(),
                q.CheckMax == null ? "" : q.CheckMax.Value.ToString(),
                q.CheckMax== null && q.CheckMin ==null ? "N/A" :  (q.CheckMax - q.CheckMin).Value.ToString(),
                q.CheckMin!=null && q.CheckMin.GetValueOrDefault() > q.ShiftMin ? true : false,
                q.CheckMax!=null &q.CheckMax.GetValueOrDefault() < q.ShiftMax ? true : false,
              }).ToList();

            return Json(new
            {
                result = result,
                Totalslot = count - 1,
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadCalendar(int storeId, String startTime, String endTime, int brandId)
        {
            var storeApi = new StoreApi();
            var store = storeApi.GetAll().Where(s => s.Id == storeId);
            var storeModel = new storeHour();
            storeModel.start = 4;
            storeModel.end = 23;
            var listAttendance = new List<CalendarAttendanceWeek>();
            DateTime startDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday);
            DateTime endDate = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Saturday);
            if (startTime.Length > 0)
            {
                startDate = startTime.ToDateTime();
                endDate = endTime.ToDateTime();
            }
            var timeSlotTemp = new List<TimeSlotWeek>();
            var attendanceApi = new AttendanceApi();
            var attenTime = attendanceApi.GetAttendanceByTimeRangeAndStore(storeId, startDate, endDate).OrderByDescending(q => q.Date).ThenByDescending(q => q.ShiftMin).GroupBy(q => q.Date).ToList();
            var attenTimeAsList = attendanceApi.GetAttendanceByTimeRangeAndStore(storeId, startDate, endDate).ToList();
            var tmpDate = startDate;
            var numEmp = new List<List<int[]>>();

            var orderedattenTimeAsList = attenTimeAsList.OrderBy(q => q.Date).ThenBy(q => q.ShiftMin);
            foreach (var att in orderedattenTimeAsList)
            {
                var numEmpDay = new List<int[]>();
                for (var i = 4; i < 23; i++)
                {
                    var startTimeParse = new TimeSpan(i, 0, 0);
                    var endTimeParse = startTimeParse.Add(new TimeSpan(0, 59, 59));
                    var empInHour = orderedattenTimeAsList.Where(q => !(q.ShiftMin > endTimeParse && q.ShiftMin > startTimeParse) && !(startTimeParse > q.ShiftMin && startTimeParse > q.ShiftMax) && q.Date == att.Date);
                    var colorTime = 0;
                    var checkRepeat = new bool[] { false, false };
                    foreach (var emp in empInHour)
                    {
                        //if (( emp.ShiftMax.Value.Subtract(new TimeSpan(0, 59, 59)).Hours == i && emp.ShiftMax.Value.Minutes == 0) || (emp.ShiftMax.Value.Hours == i && emp.ShiftMax.Value.Minutes != 0))
                        if (emp.ShiftMax.Value.Hours == i)
                        {
                            colorTime = 2;
                            checkRepeat[0] = true;
                        }
                        else if (emp.ShiftMin.Value.Hours == i)
                        {
                            colorTime = 1;
                            checkRepeat[1] = true;
                        }
                    }
                    colorTime = checkRepeat[0] && checkRepeat[1] ? 3 : colorTime;
                    numEmpDay.Add(new int[] { empInHour.Count(), colorTime });
                }
                numEmp.Add(numEmpDay);
                tmpDate = tmpDate.AddDays(1);
            }
            DateTime itemLastDay = new DateTime();
            foreach (var item in attenTime)
            {
                var attendance = new CalendarAttendanceWeek();
                var date = item.Key.Value;
                var startDateInDate = new TimeSpan(0, 0, 0);
                var endDateIndate = new TimeSpan(23, 59, 59);
                attendance.date = item.Key.Value.DayOfWeek.ToString();
                var list = attendanceApi.GetAttendanceByTimeRangeAndStore(storeId, startDateInDate, endDateIndate, date).GroupBy(x => x.ShiftMin).ToList();
                var timeSlot = new List<TimeSlotWeek>();
                foreach (var itemTime in list)
                {
                    var time = new TimeSlotWeek();
                    var total = attendanceApi.GetAttendanceByTimeRangeAndStore(storeId, startDateInDate, endDateIndate, date).Where(p => p.ShiftMin == itemTime.Key).Count();
                    time.totalEmp = "" + total;
                    time.dateSlot = item.Key.Value.ToString("dd'/'MM'/'yyyy");
                    var strEndAttendance = itemTime.FirstOrDefault().Date.Value.ToString("dd/MM/yyyy") + " " + itemTime.FirstOrDefault().ShiftMax;
                    var strStartAttendance = itemTime.FirstOrDefault().Date.Value.ToString("dd/MM/yyyy") + " " + itemTime.FirstOrDefault().ShiftMin;
                    var endTimeInDate = Utils.ToDateTimeHourSeconds(strEndAttendance);
                    var startTimeInDate = Utils.ToDateTimeHourSeconds(strStartAttendance);
                    time.startTime = startTimeInDate.ToString("dd/MM/yyyy HH:mm");
                    time.endTime = endTimeInDate.ToString("dd/MM/yyyy HH:mm");
                    TimeSpan durationLast; // Khoảng thời gian nửa dưới của ca xuyên ngày VD: Ca 23h-4h => 23h-24h
                    TimeSpan durationFirst;// Khoảng thời gian nửa trên của ca xuyên ngày VD: Ca 23h-4h => 00h-04h
                    if (startTimeInDate.Date != endTimeInDate.Date)
                    {
                        durationLast = startTimeInDate.GetEndOfDate() - startTimeInDate;
                        durationFirst = endTimeInDate - endTimeInDate.GetStartOfDate();
                        double durationLastTofloat = Math.Round(durationLast.TotalHours, 1);
                        double durationFirstTofloat = Math.Round(durationFirst.TotalHours, 1);
                        time.duration = durationLastTofloat;
                        double starHour = Math.Round(time.startTime.ToDateTime().Hour - storeModel.start, 1);
                        time.startHour = starHour;
                        if (starHour >= 18)
                        {
                            time.color = "#5CB85C";
                        }
                        else if (starHour >= 12)
                        {
                            time.color = "#F0AD4E";
                        }
                        else
                        {
                            time.color = "#5BC0DE";
                        }
                        if (timeSlotTemp.Count > 0)
                        {
                            timeSlot.Add(timeSlotTemp[0]);
                            timeSlotTemp.RemoveAt(0);
                        }
                        timeSlot.Add(time);
                        TimeSlotWeek time2 = new TimeSlotWeek
                        {
                            duration = durationFirstTofloat,
                            status = time.status,
                            dateSlot = endTimeInDate.Date.ToString("dd/MM/yyyy"),
                            startHour = endTimeInDate.GetStartOfDate().Hour,
                            color = time.color,
                            idTimeFrame = time.idTimeFrame,
                            totalEmp = time.totalEmp,
                            startTime = time.startTime,
                            endTime = time.endTime
                        };
                        timeSlotTemp.Add(time2);

                    }
                    else
                    {
                        durationLast = endTimeInDate - startTimeInDate;
                        double durationtofloat = Math.Round(durationLast.TotalHours, 1);
                        time.duration = durationtofloat;
                        double starHour = Math.Round(DateTime.Parse(time.startTime).Hour - storeModel.start, 1);
                        time.startHour = starHour;
                        if (starHour + storeModel.start >= 18)
                        {
                            time.color = "#5CB85C";
                        }
                        else if (starHour + storeModel.start >= 12)
                        {
                            time.color = "#F0AD4E";
                        }
                        else
                        {
                            time.color = "#5BC0DE";
                        }
                        if (timeSlotTemp.Count > 0)
                        {
                            timeSlot.Add(timeSlotTemp[0]);
                            timeSlotTemp.RemoveAt(0);
                        }
                        timeSlot.Add(time);
                    }
                }
                attendance.timeslot = timeSlot;
                listAttendance.Add(attendance);
                itemLastDay = item.Key.Value;
                if (timeSlotTemp.Count > 0)
                {
                    var attendance2 = new CalendarAttendanceWeek();
                    attendance2.date = itemLastDay.AddDays(1).DayOfWeek.ToString();
                    attendance2.timeslot = timeSlotTemp;
                    listAttendance.Add(attendance2);
                    //timeSlotTemp.RemoveAt(0);
                }
            }
            return Json(new { rs = listAttendance, listEmp = numEmp, storeModel = storeModel }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAttendanceStt(int stt, int attId)
        {
            var attApi = new AttendanceApi();
            var att = attApi.Get(attId);
            bool rs = false;
            try
            {
                att.Status = stt;
                //att.ProcessingStatus = 3;
                var user = HttpContext.User;
                att.UpdatePerson = user.Identity.Name;
                attApi.Edit(attId, att);
                rs = true;
            }
            catch (Exception e)
            {
                rs = true;
            }
            return Json(new { rs = rs }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateAllAttendanceStt(String strChooseDay)
        {
            if (strChooseDay.Trim().Count() > 0)
            {
                var chooseDayDate = strChooseDay.ToDateTime();
                var attApi = new AttendanceApi();
                var listAttendanceNeedUpdated = attApi.GetAllAttendanceActive().Where(a => a.Status == (int)AttendanceStatusEnum.Waiting
                                                                                          && !(a.CheckMin.GetValueOrDefault() > a.ShiftMin || a.CheckMax.GetValueOrDefault() < a.ShiftMax)
                                                                                          && a.Date == chooseDayDate).Select(a => new AttendanceViewModel
                                                                                          {
                                                                                              Id = a.Id,
                                                                                              CheckMax = a.CheckMax,
                                                                                              CheckMin = a.CheckMin,
                                                                                              Date = a.Date,
                                                                                              EmployeeId = a.EmployeeId,
                                                                                              ExpandTime = a.ExpandTime,
                                                                                              ShiftMax = a.ShiftMax,
                                                                                              ShiftMin = a.ShiftMin,
                                                                                              UpdatePerson = a.UpdatePerson,
                                                                                              Status = a.Status,
                                                                                              IsActive = a.IsActive
                                                                                          }).ToList();
                if (listAttendanceNeedUpdated.Count() > 0)
                {
                    for (int i = 0; i < listAttendanceNeedUpdated.Count(); i++)
                    {
                        var attendance = listAttendanceNeedUpdated[i];
                        try
                        {
                            attendance.Status = (int)AttendanceStatusEnum.Accept;
                            //att.ProcessingStatus = 3;
                            attendance.UpdatePerson = "StoreManagerCodeCung";
                            attApi.Edit(attendance.Id, attendance);
                        }
                        catch (Exception e)
                        {
                            return Json(new { message = "Có lỗi xảy ra" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { message = "Cập nhật thành công" }, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { message = "Không có dữ liệu hợp lệ để cập nhật" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { message = "Không có dữ liệu để cập nhật" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadAttendance(JQueryDataTableParamModel param, String strChooseDay, int status, int isLate, int typeShift)
        {
            int count = 0;
            count = param.iDisplayStart + 1;
            var attendanceApi = new AttendanceApi();
            var employeeApi = new EmployeeApi();
            var empStore = employeeApi.GetAllEmpoyeeByStoreId(1);
            var listAttendance = attendanceApi.GetAllAttendanceActive();

            if (strChooseDay.Trim().Count() > 0)
            {
                var strChooseDayDate = strChooseDay.ToDateTime();
                listAttendance = listAttendance.Where(att => att.Date == strChooseDayDate);
            }
            if (status != -1)
            {
                listAttendance = listAttendance.Where(att => att.Status == status);
            }
            //Đi trễ
            if (isLate == 0)
            {
                listAttendance = listAttendance.Where(att => att.CheckMin != null && att.CheckMin.GetValueOrDefault() > att.ShiftMin);
            }
            //Về sớm
            if (isLate == 1)
            {
                listAttendance = listAttendance.Where(att => att.CheckMax != null && att.CheckMax.GetValueOrDefault() < att.ShiftMax);
            }
            //Đúng giờ
            if (isLate == 2)
            {
                listAttendance = listAttendance.Where(att => !(att.CheckMin.GetValueOrDefault() > att.ShiftMin || att.CheckMax.GetValueOrDefault() < att.ShiftMax));
            }
            if (typeShift == 0)
            {
                listAttendance = listAttendance.Where(q => q.WorkingShiftType == (int)WorkingShiftTypeEnum.CaChinh).ToList();
            }
            if (typeShift == 1)
            {
                listAttendance = listAttendance.Where(q => q.WorkingShiftType == (int)WorkingShiftTypeEnum.CaOT).ToList();
            }
            var result = listAttendance
                        .OrderBy(att => att.CheckMin)
                        //.Skip(param.iDisplayStart)
                        //.Take(param.iDisplayLength)
                        .ToList();
            var list = result.Select(a => new object[] {
                count++,
                a.Id,
                a.Employee.EmployeeName,
                a.Date.GetValueOrDefault().ToShortDateString(),
                a.CheckMin !=null ? a.CheckMin.GetValueOrDefault().ToString() : "",
                a.CheckMax !=null ? a.CheckMax.GetValueOrDefault().ToString() : "",
                a.ShiftMin.GetValueOrDefault().ToString(),
                a.ShiftMax.GetValueOrDefault().ToString(),
                a.CheckMin.GetValueOrDefault() > a.ShiftMin ? true : false,
                a.CheckMax.GetValueOrDefault() < a.ShiftMax ? true : false,
                a.Status,
                a.UpdatePerson
            });
            var totalRecords = listAttendance.Count();
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = list
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetShiftList(String strFromDate, String strToDate)
        {
            DateTime fromDate = DateTime.Parse(strFromDate);
            DateTime toDate = DateTime.Parse(strToDate);
            AttendanceApi attendanceApi = new AttendanceApi();
            var listShift = attendanceApi.GetAllAttendanceActive().Where(a => BetweenDate(a.Date.GetValueOrDefault(), fromDate, toDate)
                                                                        && a.CheckMax == null
                                                                        && a.CheckMin == null
                                                                        ).Select(att => new AttendanceEditViewModel
                                                                        {
                                                                            Id = att.Id,
                                                                            EmployeeName = att.Employee.EmployeeName,
                                                                            Date = att.Date,
                                                                            ShiftMax = att.ShiftMax,
                                                                            ShiftMin = att.ShiftMin,
                                                                        });
            if (listShift != null)
            {
                return Json(new { success = true, list = listShift }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }
        public static bool BetweenDate(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2 || input == date1 || input == date2);
        }

    }
    public class AttendanceEditViewModel
    {
        public int Id { get; set; }
        public String EmployeeName { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.TimeSpan> ShiftMin { get; set; }
        public Nullable<System.TimeSpan> ShiftMax { get; set; }
    }
    public class storeHour
    {
        public double start;
        public double end;
    }
    public class CalendarAttendanceWeek
    {
        public string date;
        public List<TimeSlotWeek> timeslot;
    }
    public class TimeSlotWeek
    {
        public double duration;
        public double startHour;
        public string status;
        public string color;
        public int idTimeFrame;
        public string dateSlot;
        public string totalEmp = "";
        public string startTime;
        public string endTime;
    }
}