﻿using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HmsService.Sdk;
using HmsService.ViewModels;
using HmsService.Models;
using HmsService.Models.Entities;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class TimeModeController : Controller
    {
        // GET: TimeMode
        public ActionResult Index()
        {
            DayTypeApi dayTypeApi = new DayTypeApi();
            var dayTypeList = dayTypeApi.Get().Where(dayType => dayType.IsActive).Select(dayType => new SelectListItem
            {
                Text = dayType.DayTypeName,
                Value = dayType.Id.ToString()
            });
            TimeModeEditViewModel timeModeEditViewModel = new TimeModeEditViewModel()
            {
                AvailableTimeModeList = dayTypeList
            };

            return View("Index", timeModeEditViewModel);
            //return View();
        }

        public ActionResult Create()
        {
            DayTypeApi dayTypeApi = new DayTypeApi();
            var dayTypeList = dayTypeApi.Get().Where(dayType => dayType.IsActive).Select(dayType => new SelectListItem
            {
                Text = dayType.DayTypeName,
                Value = dayType.Id.ToString()
            });
            TimeModeEditViewModel timeModeEditViewModel = new TimeModeEditViewModel()
            {
                AvailableTimeModeList = dayTypeList
            };

            return View("Create", timeModeEditViewModel);
            //return View();
        }


        [HttpPost]
        public async Task<JsonResult> EditTimeModeAsync(String id, String timeModeName, String timeStart, String timeEnd, String durationMin, String durationMax,/* String rateValue,*/ String dayTypeId/*, String priorityId*/)
        {
            bool result = false;
            try
            {
                var timeModeApi = new TimeModeApi();
                var timeMode = await timeModeApi.GetAsync(Int32.Parse(id));
                timeMode.TimeModeName = timeModeName;
                //timeMode.TimeStart = TimeSpan.ParseExact(timeStart, "hh:mm:ss", null);
                timeMode.TimeStart = TimeSpan.Parse(timeStart);
                timeMode.TimeEnd = TimeSpan.Parse(timeEnd);
                timeMode.DurationMin = TimeSpan.Parse(durationMin);
                timeMode.DurationMax = TimeSpan.Parse(durationMax);
                //timeMode.RateValue = Double.Parse(rateValue);
                timeMode.DayTypeId = Int32.Parse(dayTypeId);
                //timeMode.Priority = Int32.Parse(priorityId);
                await timeModeApi.EditAsync(Int32.Parse(id), timeMode);
                result = true;

            }
            catch (Exception ex)
            {
                result = false;
            }
            return Json(result);
        }
        [HttpPost]
        public async Task<JsonResult> DeleteTimeModeAsync(String id)
        {
            bool result = false;
            try
            {
                var timeModeApi = new TimeModeApi();
                var timeMode = await timeModeApi.GetAsync(Int32.Parse(id));
                if (timeMode.IsActive)
                {
                    timeMode.IsActive = false;
                    await timeModeApi.EditAsync(Int32.Parse(id), timeMode);
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult CreateTimeMode(TimeModeEditViewModel timeModeEditViewModel)
        {
            try
            {
                timeModeEditViewModel.TimeModeViewModel.IsActive = true;
                timeModeEditViewModel.TimeModeViewModel.Priority = (int)timeModeEditViewModel.TimeModePriorityEnum;
                var timeModeApi = new TimeModeApi();
                //await timeModeApi.CreateAsync(timeModeEditViewModel.TimeModeViewModel);
                timeModeApi.Create(timeModeEditViewModel.TimeModeViewModel);
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("Index");
        }

        public JsonResult GetTimeMode(HmsService.Models.JQueryDataTableParamModel param)
        {
            try
            {
                var timeModeApi = new TimeModeApi();
                var listTimeMode = timeModeApi.GetAll().ToList();

                var count = 0;
                var wks = listTimeMode.Select(e => new object[]
                {
                ++count,
                e.Id,
                e.TimeModeName!= null ? e.TimeModeName : "",
                e.TimeStart!= null ? e.TimeStart.GetValueOrDefault().ToString(@"hh\:mm") : "",
                e.TimeEnd!= null ? e.TimeEnd.GetValueOrDefault().ToString(@"hh\:mm") : "",
                e.DurationMin != null ? e.DurationMin.GetValueOrDefault().ToString(@"hh\:mm"): "",
                e.DurationMax != null ? e.DurationMax.GetValueOrDefault().ToString(@"hh\:mm"): "",
                //e.RateValue !=null ? e.RateValue :0,
                e.DayTypeId,
                e.DayType.DayTypeName,
                //((PriorityEnum)e.Priority),
                //((PriorityEnum)e.Priority).DisplayName(),
                }).ToList();

                var totalRecords = wks.Count();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = totalRecords,
                    aaData = wks
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}