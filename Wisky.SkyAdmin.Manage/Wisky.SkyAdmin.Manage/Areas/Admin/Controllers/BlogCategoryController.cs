﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Controllers;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class BlogCategoryController : DomainBasedController
    {
        // GET: Admin/BlogCategory
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ValidateSeoName(string seoName, int id)
        {
            var blogCateApi = new BlogCategoryApi();
            var checkSeo = blogCateApi.BaseService.FirstOrDefault(q => q.Id != id && q.SeoName.ToLower().Equals(seoName.ToLower()));
            if (checkSeo != null)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }

        }
        public ActionResult IndexList(JQueryDataTableParamModel param, int brandId, int parentCategory, int productFilter)
        {
            var blogCateApi = new BlogCategoryApi();
            IQueryable<BlogCategory> allBlogCate = blogCateApi.GetAllBlogCategoryActive().OrderBy(q => q.Position);
            if (productFilter == 1) // Filter Bài Display 
            {
                allBlogCate = allBlogCate.Where(q => q.IsDisplay == true);
            }
            if (productFilter == 2) // Filter Bài Display 
            {
                allBlogCate = allBlogCate.Where(q => q.IsDisplay == false);
            }
            var totalRecords = allBlogCate.Count();
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                allBlogCate = allBlogCate.Where(q => q.BlogCateName.ToLower().Equals(param.sSearch.ToLower()));
            }
            if (parentCategory > 0)
            {
                allBlogCate = allBlogCate.Where(q => q.ParentCateId == parentCategory);
            }
            else
            {
                allBlogCate = allBlogCate.Where(q => q.ParentCateId == null);
            }
            var totalDisplay = allBlogCate.Count();
            var startDisplay = param.iDisplayStart + 1;
            var result = allBlogCate.OrderBy(q => q.Position).Skip(param.iDisplayStart).Take(param.iDisplayLength).Select(q => new
            {
                PicUrl = q.PicUrl,
                CateName = q.BlogCateName,
                CateNameEn = q.BlogCateName_EN,
                NumberBlogs = q.BlogPosts.Where(a => a.Active).Count(),
                Position = q.Position,
                PositionHome = q.PositionTopicHomePage,
                IsDisplay = q.IsDisplay,
                Active = q.IsActive,
                Id = q.Id
            }).ToList();
            var count = startDisplay;
            var returnResult = result.Select(q => new IConvertible[]
            {
                count++,
                q.PicUrl,
                q.CateName,
                q.CateNameEn,
                q.NumberBlogs,
                q.Position,
                q.PositionHome,
                q.IsDisplay,
                q.Active,
                q.Id
            });
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalDisplay,
                aaData = returnResult
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllParrentCate(int brandId)
        {
            var blogCateApi = new BlogCategoryApi();
            var listParentCate = blogCateApi.GetParrentCategory(brandId).ToList();
            var result = listParentCate.Select(q => new IConvertible[]
            {
               q.Id,
               q.BlogCateName
            }).ToList();
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int brandId, int id)
        {
            var blogCateApi = new BlogCategoryApi();
            var blogCate = blogCateApi.BaseService.Get(id);
            if (blogCate != null)
            {
                blogCate.IsActive = false;
                try
                {
                    blogCateApi.BaseService.Update(blogCate);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = "Có lỗi xảy ra trong quá trình xử lý, vui lòng thử lại" });
                }
            }
            else
            {
                return Json(new { success = false, message = "Không Tồn Tại Chuyên Mục Này" });
            }
            return Json(new { success = true, message = "Xóa thành công" });
        }

        public JsonResult LoadParentCategory(int brandId)
        {
            var blogCateapi = new BlogCategoryApi();
            var listBlogCate = blogCateapi.GetAllBlogCategoryActive().Where(q => q.IsDisplay == true).Select(q => new
            {
                q.Id,
                q.BlogCateName
            }).ToArray();
            return Json(new
            {
                success = true,
                blogCateList = listBlogCate,
            }, JsonRequestBehavior.AllowGet);
        }

        public void PrepareCreate(BlogCategoryViewModel model)
        {
            model.IsActive = true;
            var availableBlogCate = (new BlogCategoryApi()
                .GetAllBlogCategoryActiveByBrandId(this.CurrentStore.BrandId.Value)).Where(q => q.ParentCateId == null);
            model.AvailableBlogCategories = availableBlogCate.ToSelectList(q => q.BlogCateName, q => q.Id.ToString(), q => q.Id == model.ParentCateId);

        }

        public ActionResult Create(int brandId, int typeCate)
        {
            var model = new BlogCategoryViewModel();
            model.Type = typeCate;
            model.IsActive = true;
            model.IsDisplay = false;
            PrepareCreate(model);
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Create(BlogCategoryViewModel model, int brandId)
        {
            if (model != null)
            {
                var entity = model.ToEntity();
                var blogCategoryApi = new BlogCategoryApi();
                try
                {
                    var existSeoname = blogCategoryApi.Get().Where(b => b.SeoName == entity.SeoName).FirstOrDefault();
                    if (existSeoname != null)
                    {
                        List<string> notification = new List<string>();
                        notification.Add("Seoname đã tồn tại");
                        ViewData["Notification"] = notification;
                        return View(model);
                    }
                    entity.IsActive = true;
                    if (entity.IsDisplay == null)
                    {
                        entity.IsDisplay = false;
                    }
                    if (model.ParentCateId != null && model.ParentCateId > 0)
                    {
                        entity.ParentCateId = model.ParentCateId;
                    }
                    entity.IsAllowComment = "false";
                    entity.StoreId = 2;
                    entity.BrandId = 1;
                    entity.Description = entity.Description;
                    entity.Description_EN = entity.Description_EN;
                    blogCategoryApi.BaseService.Create(entity);
                    return View("Index");
                }
                catch (Exception ex)
                {
                    return View(model);
                }

            }
            else
            {
                return View(model);
            }

        }

        public void PrepareEdit(BlogCategoryViewModel model)
        {
            model.IsActive = true;
            var availableBlogCate = (new BlogCategoryApi()
                .GetAllBlogCategoryActiveByBrandId(this.CurrentStore.BrandId.Value)).Where(q => q.ParentCateId == null);
            model.AvailableBlogCategories = availableBlogCate.ToSelectList(q => q.BlogCateName, q => q.Id.ToString(), q => q.Id == model.ParentCateId);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">cateId</param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            var info = new BlogCategoryApi()
               .Get(id);

            if (info == null)
            {
                return this.IdNotFound();
            }

            this.PrepareEdit(info);
            return this.View(info);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Edit(BlogCategoryViewModel blogCateVM)
        {
            var api = new BlogCategoryApi();
            // Validate
            var info = api
                .BaseService.Get(blogCateVM.Id);

            if (info == null)
            {
                return this.IdNotFound();
            }

            if (!this.ModelState.IsValid)
            {
                this.PrepareEdit(blogCateVM);
                return this.View(blogCateVM);
            }
            info.IsDisplay = blogCateVM.IsDisplay;
            info.SeoName = blogCateVM.SeoName;
            info.SeoDescription = blogCateVM.SeoDescription;
            info.SeoKeyword = blogCateVM.SeoKeyword;
            info.BlogCateName = blogCateVM.BlogCateName;
            info.BlogCateName_EN = blogCateVM.BlogCateName_EN;
            info.PageTitle = blogCateVM.PageTitle;
            info.PageTitle_EN = blogCateVM.PageTitle_EN;
            info.IsActive = true;
            if (blogCateVM.ParentCateId > 0)
            {
                info.ParentCateId = blogCateVM.ParentCateId;
            }
            info.PicUrl = blogCateVM.PicUrl;
            info.Description = blogCateVM.Description;
            info.Description_EN = blogCateVM.Description_EN;
            info.Position = blogCateVM.Position;
            info.PositionTopicHomePage = blogCateVM.PositionTopicHomePage;
            api.BaseService.Update(info);
            return this.RedirectToAction("Index", new { parameters = this.CurrentPageDomain.Directory });
        }
    }
}