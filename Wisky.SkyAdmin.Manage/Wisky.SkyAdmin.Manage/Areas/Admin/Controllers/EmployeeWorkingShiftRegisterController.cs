﻿using HmsService.Sdk;
using HmsService.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wisky.SkyAdmin.Manage.Areas.Admin.Controllers
{
    public class EmployeeWorkingShiftRegitserController : Controller
    {
        // GET: EmployeeWorkingShiftRegitser
        public ActionResult Index()
        {
            EmployeeWorkingShiftRegitserApi employeeWorkingShiftApi = new EmployeeWorkingShiftRegitserApi();
            employeeWorkingShiftApi.GetAll();
            return View(employeeWorkingShiftApi.GetAll().ToList());
        }
        public ActionResult Create(Message[] listMessage)
        {
            try
            {
                foreach (var message in listMessage)
                {
                    foreach (var slotId in message.slotId)
                    {
                        EmployeeWorkingShiftRegitserApi employeeWorkingShiftRegisterApi = new EmployeeWorkingShiftRegitserApi();
                        employeeWorkingShiftRegisterApi.Create(new EmployeeWorkingShiftRegitserViewModel
                        {
                            Date = DateTime.Parse(message.Date),
                            EmployeeId = message.employeeId,
                            WorkingShiftId = slotId,
                            TimeCreated = DateTime.Now,
                            IsActive = true,
                        });
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                success = true
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllShiftRegister(String strFromDate, String strToDate, int empId)
        {
            DateTime fromDate = DateTime.Parse(strFromDate);
            DateTime toDate = DateTime.Parse(strToDate);
            EmployeeWorkingShiftRegitserApi employeeWorkingShiftRegitserApi = new EmployeeWorkingShiftRegitserApi();
            var shiftRegisters = employeeWorkingShiftRegitserApi.GetAll().Where(s => s.IsActive == true
                                                                                && s.Date.GetValueOrDefault() >= fromDate
                                                                                && s.Date.GetValueOrDefault() <= toDate
                                                                                && s.EmployeeId == empId)
                                                                                .Select(s => new EmployeeWorkingShiftRegitserViewModel
                                                                                {
                                                                                    Id = s.Id,
                                                                                    Date = s.Date,
                                                                                    EmployeeId = s.EmployeeId,
                                                                                    TimeCreated = s.TimeCreated,
                                                                                    WorkingShiftId = s.WorkingShiftId,
                                                                                    IsActive = s.IsActive
                                                                                });
            return Json(new
            {
                success = true,
                list = shiftRegisters
            }, JsonRequestBehavior.AllowGet);
        }
    }
    public class Message
    {
        public int employeeId { get; set; }
        public string Date { get; set; }
        public List<int> slotId { get; set; }
    }
}