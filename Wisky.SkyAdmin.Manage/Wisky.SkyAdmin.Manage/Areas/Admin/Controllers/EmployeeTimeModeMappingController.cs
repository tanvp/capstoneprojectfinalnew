﻿using HmsService.Sdk;
using HmsService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Capstone_KPS.Areas.Admin.Controllers
{
        // GET: Admin/EmployeeTimeModeMapping
        public class EmployeeTimeModeMappingController : Controller
        {
            // GET: EmployeeTimeModeMapping
            public ActionResult Index()
            {
                return View();
            }
            public JsonResult GetEmpTmMap(JQueryDataTableParamModel param, String strFromDate, String strToDate, int empId)
            {
                try
                {
                    DateTime fromDate = strFromDate.ToDateTime();
                    DateTime toDate = strToDate.ToDateTime();
                    var empTmMap = new EmployeeTimeModeMappingApi();
                    var listEmpTmMap = empTmMap.GetAll();
                    if (fromDate != null && toDate != null)
                    {
                        listEmpTmMap = empTmMap.GetAll().Where(e => e.Date >= fromDate
                                                                    && e.Date <= toDate
                                                                    && e.EmployeeId == empId);
                    }
                    var salaryLevelApi = new SalaryLevelApi();
                    var count = 0;
                    var emTm = listEmpTmMap.OrderBy(e => e.Date).Select(e => new object[]
                      {
                    ++count,
                    e.Date.GetValueOrDefault().ToShortDateString(),
                    e.TimeMode.TimeModeName,
                    e.TotalHours.GetValueOrDefault().ToString(@"hh\:mm"),
                      }).ToList();

                    var totalRecords = emTm.Count();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalRecords = totalRecords,
                        iTotalDisplayRecords = totalRecords,
                        aaData = emTm
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Không Thể Tải");
                }
            }
            public JsonResult GetEmpTmMapTotal(JQueryDataTableParamModel param, String strFromDate, String strToDate, int empId)
            {
                try
                {
                    DateTime fromDate = strFromDate.ToDateTime();
                    DateTime toDate = strToDate.ToDateTime();
                    var empTmMap = new EmployeeTimeModeMappingApi();
                    var listEmpTmMap = empTmMap.GetAll();
                    if (fromDate != null && toDate != null)
                    {
                        listEmpTmMap = empTmMap.GetAll().Where(e => e.Date >= fromDate
                                                                    && e.Date <= toDate
                                                                    && e.EmployeeId == empId);
                    }
                    var salaryLevelApi = new SalaryLevelApi();
                    var count = 0;
                    var emTm = listEmpTmMap.GroupBy(e => e.TimeModeId).Select(e => new object[]
                      {
                    ++count,
                    e.First().Date.GetValueOrDefault().ToShortDateString(),
                    e.FirstOrDefault().TimeMode.TimeModeName,
                    e.Aggregate(new TimeSpan(), (sum, nextData) => sum.Add(nextData.TotalHours.GetValueOrDefault())).ToString(@"hh\:mm"),
                    }).ToList();

                    var totalRecords = emTm.Count();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalRecords = totalRecords,
                        iTotalDisplayRecords = totalRecords,
                        aaData = emTm
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Không Thể Tải");
                }
            }

            public JsonResult GetEmpTmMapTotalByDate(JQueryDataTableParamModel param, String username, String strDate)
            {
                try
                {
                    DateTime date = (strDate + "/2018").ToDateTime();
                    var empTmMap = new EmployeeTimeModeMappingApi();
                    var listEmpTmMap = empTmMap.GetAll();
                    if (date != null)
                    {
                        listEmpTmMap = empTmMap.GetAll().Where(e => e.Date == date
                                                                    && e.Employee.Username.Equals(username));
                    }
                    var salaryLevelApi = new SalaryLevelApi();
                    var count = 0;
                    var emTm = listEmpTmMap.Select(e => new object[]
                      {
                    ++count,
                    e.Date.GetValueOrDefault().ToShortDateString(),
                    e.TimeMode.TimeModeName,
                    e.TotalHours.GetValueOrDefault().ToString(@"hh\:mm"),
                    }).ToList();

                    var totalRecords = emTm.Count();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalRecords = totalRecords,
                        iTotalDisplayRecords = totalRecords,
                        aaData = emTm
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json("Không Thể Tải");
                }
            }
            public ActionResult GetTimekeeping(int storeSelected, string startDate, string endDate, int brandId)
            {

                var startTime = startDate.ToDateTime();
                var endTime = endDate.ToDateTime();

                var employeeTimeModeMappingApi = new EmployeeTimeModeMappingApi();
                var employeeApi = new EmployeeApi();
                var storeApi = new StoreApi();

                var listEmployees = storeSelected != 0
                    ? employeeApi.GetAllEmpoyeeByStoreId(storeSelected)
                    : employeeApi.GetAll().ToList();
                var totalRecord = listEmployees.Count();

                var totalResult = listEmployees.Count();

                var listEmployeeTimeModeMapping = storeSelected != 0
                    ? employeeTimeModeMappingApi.GetByStoreByTimeRange(storeSelected, startTime, endTime)
                    : employeeTimeModeMappingApi.GetByTimeRangeAndBrand(storeApi.GetActiveStoreByBrandId(brandId).Select(q => q.Id).ToList(), startTime, endTime);
                //join 2 list Employee và Attendace => ta có các attendance đi theo employee

                var listjoin = from tableEmp in listEmployees
                               join tableEmpTmp in listEmployeeTimeModeMapping
                               on tableEmp.Id equals tableEmpTmp.EmployeeId into ps
                               from tableEmpTmp in ps.DefaultIfEmpty()
                               select new
                               {
                                   Id = tableEmp.Id,
                                   UserName = tableEmp.Username,
                                   Name = tableEmp.EmployeeName,
                                   EmployeeTimemodeMapping = tableEmpTmp
                               };

                //-> list Employee mà mỗi employee có list att theo nó
                var listFinalJoin = listjoin.GroupBy(q => q.Id);

                var listStringDate = new List<String>();

                for (DateTime i = startTime; i <= endTime; i = i.AddDays(1))
                {
                    listStringDate.Add(i.ToString("dd/MM"));
                }
                //listStringDate.Add();

                var listResult = new List<EmployeeTimemodeReportViewModel>();
                var displayStart = 1;
                foreach (var item in listFinalJoin)
                {
                    var report = new EmployeeTimemodeReportViewModel(startTime, endTime);
                    report.No = displayStart++;
                    var firtAttendance = item.FirstOrDefault();
                    report.StoreName = storeApi.Get(storeSelected).Name;
                    report.EmployeeName = firtAttendance.Name;
                    report.EmpID = firtAttendance.Id;
                    report.Username = firtAttendance.UserName;
                    if (firtAttendance.EmployeeTimemodeMapping != null)
                    {
                        // Have Attendance 
                        foreach (var empTmp in item)
                        {
                            if (empTmp.EmployeeTimemodeMapping != null)
                            {
                                var date = empTmp.EmployeeTimemodeMapping.Date;
                                if (empTmp.EmployeeTimemodeMapping != null)
                                {
                                    var reportTmp = report.Month.Where(q => q.DateString.Equals(empTmp.EmployeeTimemodeMapping.Date.GetValueOrDefault().ToString("dd/MM"))).FirstOrDefault();
                                    reportTmp.timework = reportTmp.timework.Add(empTmp.EmployeeTimemodeMapping.TotalHours.Value);
                                }
                            }
                        }
                    }

                    listResult.Add(report);
                }
                var finalResult = new List<string[]>();

                foreach (var item in listResult.OrderBy(q => q.StoreName).ToList())
                {
                    var tmpResult = new string[listStringDate.Count() + 7];
                    tmpResult[0] = "" + item.No;
                    tmpResult[1] = "" + item.Username;
                    tmpResult[2] = "" + item.StoreName;
                    tmpResult[3] = "" + item.EmployeeName;
                    var totalTime = new TimeSpan(item.Month.Sum(q => q.timework.Ticks));
                    var stringtime = "";
                    var tmp = Int32.Parse(totalTime.TotalHours.ToString().Split('.')[0]);

                    if (tmp < 10)
                    {
                        stringtime += "0" + totalTime.TotalHours.ToString().Split('.')[0];
                    }
                    else
                    {
                        stringtime += totalTime.TotalHours.ToString().Split('.')[0];
                    }

                    stringtime += ":";
                    if (totalTime.Minutes < 10)
                    {
                        stringtime += "0" + totalTime.Minutes;
                    }
                    else
                    {
                        stringtime += totalTime.Minutes;
                    }
                    var tmpMinute = int.Parse(stringtime.Split(':')[1]);
                    if (tmpMinute > 60)
                    {
                        stringtime = (int.Parse(stringtime.Split(':')[0]) + (int)(tmpMinute / 60)).ToString();
                        stringtime += ":" + (tmpMinute % 60).ToString();
                    }

                    for (int i = 0; i < item.Month.Count; i++)
                    {
                        tmpResult[i + 4] = item.Month[i].timework.Ticks != 0 ? item.Month[i].timework.Hours.ToString("00") + ":" + item.Month[i].timework.Minutes.ToString("00") : "";
                    }
                    tmpResult[item.Month.Count + 4] = stringtime;
                    tmpResult[tmpResult.Count() - 2] = "<button type='submit' onclick = 'exportOneEmployee(" + item.EmpID + ")' class='width230 btn btn-primary btn-sm waves-effect'>" +
                                                        "<i class='fa fa - download'></i>" +
                                                        "Xuất Excel</button>";

                    finalResult.Add(tmpResult);
                }

                return Json(new
                {
                    stringdate = listStringDate,
                    result = finalResult
                }, JsonRequestBehavior.AllowGet);
            }

            public ActionResult GetTimekeepingByTimemode(int storeSelected, string startDate, string endDate, int brandId)
            {

                var startTime = startDate.ToDateTime();
                var endTime = endDate.ToDateTime();
                var timeModeApi = new TimeModeApi();
                var employeeTimeModeMappingApi = new EmployeeTimeModeMappingApi();
                var employeeApi = new EmployeeApi();
                var storeApi = new StoreApi();

                var listEmployees = storeSelected != 0
                    ? employeeApi.GetAllEmpoyeeByStoreId(storeSelected)
                    : employeeApi.GetAll().ToList();
                var totalRecord = listEmployees.Count();

                var totalResult = listEmployees.Count();

                var listEmployeeTimeModeMapping = storeSelected != 0
                    ? employeeTimeModeMappingApi.GetByStoreByTimeRange(storeSelected, startTime, endTime)
                    : employeeTimeModeMappingApi.GetByTimeRangeAndBrand(storeApi.GetActiveStoreByBrandId(brandId).Select(q => q.Id).ToList(), startTime, endTime);
                //join 2 list Employee và Attendace => ta có các attendance đi theo employee

                var listjoin = from tableEmp in listEmployees
                               join tableEmpTmp in listEmployeeTimeModeMapping
                               on tableEmp.Id equals tableEmpTmp.EmployeeId into ps
                               from tableEmpTmp in ps.DefaultIfEmpty()
                               select new
                               {
                                   Id = tableEmp.Id,
                                   UserName = tableEmp.Username,
                                   Name = tableEmp.EmployeeName,
                                   EmployeeTimemodeMapping = tableEmpTmp
                               };

                //-> list Employee mà mỗi employee có list att theo nó
                var listFinalJoin = listjoin.GroupBy(q => q.Id);

                var listTimeModeName = new List<String>();
                //đã get active
                foreach (var item in timeModeApi.GetAll())
                {
                    listTimeModeName.Add(item.TimeModeName);
                }
                //listStringDate.Add();

                var listResult = new List<EmployeeTimemodeReportViewModel2>();
                var displayStart = 1;
                foreach (var item in listFinalJoin)
                {
                    var report = new EmployeeTimemodeReportViewModel2(listTimeModeName);
                    report.No = displayStart++;
                    var firtAttendance = item.FirstOrDefault();
                    report.StoreName = storeApi.Get(storeSelected).Name;
                    report.EmployeeName = firtAttendance.Name;
                    report.EmpID = firtAttendance.Id;
                    report.Username = firtAttendance.UserName;
                    if (firtAttendance.EmployeeTimemodeMapping != null)
                    {
                        // Have Attendance 
                        foreach (var empTmp in item)
                        {
                            if (empTmp.EmployeeTimemodeMapping != null)
                            {
                                var date = empTmp.EmployeeTimemodeMapping.Date;
                                if (empTmp.EmployeeTimemodeMapping != null)
                                {
                                    var reportTmp = report.Month.Where(q => q.timeModeName.Equals(empTmp.EmployeeTimemodeMapping.TimeMode.TimeModeName)).FirstOrDefault();
                                    reportTmp.timework = reportTmp.timework.Add(empTmp.EmployeeTimemodeMapping.TotalHours.Value);
                                }
                            }
                        }
                    }

                    listResult.Add(report);
                }
                var finalResult = new List<string[]>();

                foreach (var item in listResult.OrderBy(q => q.StoreName).ToList())
                {
                    var tmpResult = new string[listTimeModeName.Count() + 7];
                    tmpResult[0] = "" + item.No;
                    tmpResult[1] = "" + item.Username;
                    tmpResult[2] = "" + item.StoreName;
                    tmpResult[3] = "" + item.EmployeeName;
                    var totalTime = new TimeSpan(item.Month.Sum(q => q.timework.Ticks));
                    var stringtime = "";
                    var tmp = Int32.Parse(totalTime.TotalHours.ToString().Split('.')[0]);

                    if (tmp < 10)
                    {
                        stringtime += "0" + totalTime.TotalHours.ToString().Split('.')[0];
                    }
                    else
                    {
                        stringtime += totalTime.TotalHours.ToString().Split('.')[0];
                    }

                    stringtime += ":";
                    if (totalTime.Minutes < 10)
                    {
                        stringtime += "0" + totalTime.Minutes;
                    }
                    else
                    {
                        stringtime += totalTime.Minutes;
                    }
                    var tmpMinute = int.Parse(stringtime.Split(':')[1]);
                    if (tmpMinute > 60)
                    {
                        stringtime = (int.Parse(stringtime.Split(':')[0]) + (int)(tmpMinute / 60)).ToString();
                        stringtime += ":" + (tmpMinute % 60).ToString();
                    }

                    for (int i = 0; i < item.Month.Count; i++)
                    {
                        tmpResult[i + 4] = item.Month[i].timework.Ticks != 0 ? item.Month[i].timework.Hours.ToString("00") + ":" + item.Month[i].timework.Minutes.ToString("00") : "";
                    }
                    tmpResult[item.Month.Count + 4] = stringtime;
                    tmpResult[tmpResult.Count() - 2] = "<button type='submit' onclick = 'exportOneEmployee(" + item.EmpID + ")' class='width230 btn btn-primary btn-sm waves-effect'>" +
                                                        "<i class='fa fa - download'></i>" +
                                                        "Xuất Excel</button>";

                    finalResult.Add(tmpResult);
                }

                return Json(new
                {
                    stringTimeMode = listTimeModeName,
                    result = finalResult
                }, JsonRequestBehavior.AllowGet);
            }
            public string decimalFormat(string number)
            {
                return Convert.ToDecimal(number).ToString("#,##0,00");
            }
        }
        public class EmployeeTimemodeReportViewModel
        {
            public int No { get; set; }
            public string EmployeeName { get; set; }
            public string StoreName { get; set; }
            public int EmpID { get; set; }
            public string Username { get; set; }
            public List<ItemMonth> Month { get; set; }

            public EmployeeTimemodeReportViewModel(DateTime startDate, DateTime endDate)
            {
                Month = new List<ItemMonth>();
                for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
                {
                    var itemMonth = new ItemMonth();
                    itemMonth.DateString = i.ToString("dd/MM");
                    itemMonth.timework = new TimeSpan(0);
                    itemMonth.timeOverTime = new TimeSpan(0);
                    itemMonth.Month = i.Month;
                    itemMonth.numberdate = i.Day;
                    Month.Add(itemMonth);
                }
            }
        }

        public class EmployeeTimemodeReportViewModel2
        {
            public int No { get; set; }
            public string EmployeeName { get; set; }
            public string StoreName { get; set; }
            public int EmpID { get; set; }
            public string Username { get; set; }
            public List<ItemMonth2> Month { get; set; }

            public EmployeeTimemodeReportViewModel2(List<string> listTimeModeName)
            {
                Month = new List<ItemMonth2>();
                foreach (var timeModeName in listTimeModeName)
                {
                    var itemMonth = new ItemMonth2();
                    itemMonth.timeModeName = timeModeName;
                    itemMonth.timework = new TimeSpan(0);
                    Month.Add(itemMonth);
                }
            }
        }
        public class ItemMonth2
        {
            public string timeModeName { get; set; }
            public TimeSpan timework { get; set; }
        }
        public class ItemMonth
        {
            public string DateString { get; set; }
            public int numberdate { get; set; }
            public int Month { get; set; }
            public TimeSpan timework { get; set; }
            public TimeSpan timeOverTime { get; set; }
            public TimeSpan CheckMin { get; set; }
            public TimeSpan CheckMax { get; set; }
            public TimeSpan timeworkApproved { get; set; }
        }
    }
