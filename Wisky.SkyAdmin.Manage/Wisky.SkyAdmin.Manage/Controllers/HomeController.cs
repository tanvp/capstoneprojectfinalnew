﻿using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wisky.SkyAdmin.Manage.Models.Identity;

namespace Admin_Capstone_KPS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ShiftApi api = new ShiftApi();
            //var a = api.GetActive();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult RenderStoreNavigation(int brandId, string returnUrl = "")
        {
            ViewBag.brandId = brandId;
            var model = new List<StoreNavigationViewModel>();
            var api = new StoreApi();
            var userApi = new StoreUserApi();
            var aspNetUser = new AspNetUserApi();
            var storeUserApi = new StoreUserApi();
            var username = HttpContext.User.Identity.Name;
            var userStores = aspNetUser.GetActive().Where(u => u.UserName.Equals(username)).Select(q => q.StoreId);
            var stores = api.BaseService.Get().Where(s => s.isActive == true && s.BrandId == brandId);
            var isManager = HttpContext.User.IsInRole("BrandManager");
            if (isManager)
            {
                foreach (var store in stores)
                {
                    var modelStore = new StoreNavigationViewModel()
                    {
                        Id = store.Id,
                        Address = store.Address,
                        StoreName = store.Name,
                        //User = userApi.GetFirstStoreManager(store.ID) ?? "Chưa có nhân viên phụ trách",
                    };
                    model.Add(modelStore);
                }
            }
            else
            {
                foreach (var store in stores)
                {
                    if (userStores.Contains(store.Id))
                    {
                        var modelStore = new StoreNavigationViewModel()
                        {
                            Id = store.Id,
                            Address = store.Address,
                            StoreName = store.Name,
                            //User = userApi.GetFirstStoreManager(store.ID) ?? "Chưa có nhân viên phụ trách",
                        };
                        model.Add(modelStore);
                    }
                }
            }
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_StoreNavigationPartial", model);
        }
        public async Task<ActionResult> ChooseStore(string returnUrl, int storeId, int? brandId)
        {
            var user = System.Web.HttpContext.Current.User;
            if (storeId == 0)
            {
                if (user.IsInRole("BrandManager"))
                {
                    return Redirect("/" + brandId + "/Admin/0/WorkingShift/Index");
                }
                else
                {
                    return Redirect("/Account/Login");
                }
            }
            var api = new StoreApi();
            var stores = await api.GetStoreByID(storeId);
            if (stores == null)
            {
                return View();
            }
            ViewBag.StoreName = stores != null
                    ? stores.Name
                    : "Title";

            if (user.IsInRole("BrandManager") || user.IsInRole("StoreManager"))
            {
                return Redirect("/" + stores.BrandId + "/Admin/" + storeId + "/Attendance/Index");
            }
            return Redirect("/" + stores.BrandId + "/Admin/" + storeId + "/Attendance/Index");
        }
        [Authorize]
        public ActionResult LeftMenu()
        {
            try
            {
                return PartialView("_MenuForStore");
            }
            catch (Exception e)
            {
                return HttpNotFound();
            }
        }
    }
}