/**
 * Japanese translation for bootstrap-datetimepicker
 * Norio Suzuki <https://github.com/suzuki/>
 */
;(function($){
	$.fn.datetimepicker.dates['vi'] = {
		days: ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy", "Chủ Nhật"],
		daysShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7", "CN"],
		daysMin: ["CN", "2", "3", "4", "5", "6", "7", "CN"],
		months: ["Tháng Một", "Tháng Hai", "Tháng Ba", "Tháng Tư", "Tháng Năm", "Tháng Sáu", "Tháng Bảy", "Tháng Tám", "Tháng Chín", "Tháng Mười", "Tháng Mười Một", "Tháng Mười Hai"],
		monthsShort: ["Thg 1", "Thg 2", "Thg 3", "Thg 4", "Thg 5", "Thg 6", "Thg 7", "Thg 8", "Thg 9", "Thg 10", "Thg 11", "Thg 12"],
		today: "Hôm Nay",
		suffix: [],
		meridiem: []
	};
}(jQuery));
