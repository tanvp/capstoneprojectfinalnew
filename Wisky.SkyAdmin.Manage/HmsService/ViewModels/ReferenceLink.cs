﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class ReferenceLink
    {
        public string tile { get; set; }
        public string tile_eng { get; set; }
        public string linkAccess { get; set; }
    }
}
