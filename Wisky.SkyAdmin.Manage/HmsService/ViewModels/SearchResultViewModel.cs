﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public class SearchResultViewModel
    {
        public string KeywordSearch { get; set; }
        public int NumberResult { get; set; }
        public List<BlogPost> Results { get; set; }
        public int SearchType { get; set; }
        public SearchResultViewModel()
        {
            this.Results = new List<BlogPost>();
            this.KeywordSearch = "";
            this.NumberResult = 0;
            this.SearchType = 0;
        }
    }
}
