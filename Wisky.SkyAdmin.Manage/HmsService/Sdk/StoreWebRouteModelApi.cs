﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class StoreWebRouteModelApi
    {
        public IEnumerable<StoreWebRouteModel> GetStoreWebRouteModelByRoute(int storeWebRouteId)
        {
            return this.BaseService.Get(q => q.StoreWebRouteId == storeWebRouteId).ToList();
        }
    }
}
