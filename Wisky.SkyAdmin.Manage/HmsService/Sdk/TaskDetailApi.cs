﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class TaskDetailApi
    {
        public IEnumerable<TaskDetail> GetAllTaskByStoreByTime(int storeId, DateTime startTime, DateTime endTime)
        {
            return this.BaseService.Get(q => q.Active == true && q.StartTime >= startTime && q.EndTime <= endTime);
        }
    }
}
