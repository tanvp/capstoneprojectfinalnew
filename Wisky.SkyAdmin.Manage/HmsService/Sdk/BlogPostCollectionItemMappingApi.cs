﻿using HmsService.Models;
using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class BlogPostCollectionItemMappingApi {
        public List<BlogPostCollectionItemMapping> GetBlogPostMappingByCollection(int collectionId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            return blogPostCollectionMappingApi.BaseService.Get(q => q.BlogPostCollectionId == collectionId).ToList();
        }

        public List<BlogPostCollectionItemMapping> GetBlogPostMappingByCollectionAndSearch(int collectionId, string search)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            return blogPostCollectionMappingApi.BaseService.Get(q => q.BlogPostCollectionId == collectionId && q.BlogPost.Title.Contains(search)).ToList();
        }

        public BlogPostCollectionItemMapping GetBlogPostMappingInCollection(int collectionId, int blogPostId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            return blogPostCollectionMappingApi.BaseService.Get(q => q.BlogPostCollectionId == collectionId && q.BlogPostId == blogPostId).FirstOrDefault();
        }

        public BlogPostCollectionItemMapping GetBlogPostMappingByMappingId(int mappingId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            return blogPostCollectionMappingApi.BaseService.Get(q => q.Id == mappingId).FirstOrDefault();
        }

        public bool EditBlogPostPositionInColelction(int mappingId, int newPosition)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            var mapping = blogPostCollectionMappingApi.BaseService.Get(q => q.Id == mappingId).FirstOrDefault();
            mapping.Position = newPosition;
            if (mapping != null)
            {
                try
                {
                    blogPostCollectionMappingApi.BaseService.Update(mapping);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                    throw;
                }
            }
            return false;
        }


        public bool RemoveBlogPostFromCollection(int mappingId)
        {
            BlogPostCollectionItemMappingApi blogPostCollectionMappingApi = new BlogPostCollectionItemMappingApi();
            var mapping = blogPostCollectionMappingApi.BaseService.Get(q => q.Id == mappingId).FirstOrDefault();
            if (mapping != null)
            {
                try
                {
                    blogPostCollectionMappingApi.BaseService.Delete(mapping);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                    throw;
                }
            }
            return false;
        }

    }
}
