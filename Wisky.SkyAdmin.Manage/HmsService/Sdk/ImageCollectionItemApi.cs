﻿using HmsService.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class ImageCollectionItemApi
    {
        public List<ImageCollectionItem> GetbyCollectionId(int collectionId)
        {
            return this.BaseService.Get(q => q.ImageCollectionId == collectionId && q.Active).ToList();
        }
    }
}
